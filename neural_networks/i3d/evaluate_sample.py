# Copyright 2017 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Loads a sample video and classifies using a trained Kinetics checkpoint."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os

import numpy as np
import tensorflow as tf
import yaml

import neural_networks.i3d.i3d as i3d
import dataset.video_processing.convert as cvrt
import dataset.utils as dataset_utils

import neural_networks.utils as neural_network_utils

dir_path = os.path.dirname(os.path.realpath(__file__))

_IMAGE_SIZE = 224
_SAMPLE_VIDEO_FRAMES = 79

_SAMPLE_PATHS = {
    'rgb': 'data/v_CricketShot_g04_c01_rgb.npy',
    'flow': 'data/v_CricketShot_g04_c01_flow.npy',
    'flow2': 'data/v_Diving_g22_c04_opt.npy',
    'rgb2': 'data/v_Diving_g22_c04_rgb.npy',
}

_CHECKPOINTS_PATH = 'C:\\Users\\micha\\Documents\\Informatyka_EiTI_2018-2020\\mgr\\i3d\\data\\checkpoints'

_CHECKPOINT_PATHS = neural_network_utils.load_checkpoints_dict()
# {
#     'rgb': _CHECKPOINTS_PATH + '\\rgb_scratch\\model.ckpt',
#     'rgb600': _CHECKPOINTS_PATH + '\\rgb_scratch_kin600\\model.ckpt',
#     'flow': _CHECKPOINTS_PATH + '\\flow_scratch\\model.ckpt',
#     'rgb_imagenet': _CHECKPOINTS_PATH + '\\rgb_imagenet\\model.ckpt',
#     'flow_imagenet': _CHECKPOINTS_PATH + '\\flow_imagenet\\model.ckpt',
# }

_LABEL_MAP_PATH = 'data/label_map.txt'
_LABEL_MAP_PATH_600 = 'data/label_map_600.txt'

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string('eval_type', 'joint', 'rgb, rgb600, flow, or joint')
tf.flags.DEFINE_boolean('imagenet_pretrained', True, '')

_RGB_DIRECTORIES_PATH = ''
_FLOW_DIRECTORIES_PATH = ''
_PREDICTIONS_PATH = ''
directories_rgb = []
directories_flow = []
actions_directories = []
directories_predictions = []


def main(unused_argv):
    with open(dir_path + "/resources/i3d_config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    _RGB_DIRECTORIES_PATH = cfg['_RGB_DIRECTORIES']
    _FLOW_DIRECTORIES_PATH = cfg['_FLOW_DIRECTORIES']
    directories_rgb = dataset_utils.get_directories_in_directory(_RGB_DIRECTORIES_PATH)
    directories_flow = dataset_utils.get_directories_in_directory(_FLOW_DIRECTORIES_PATH)
    actions_directories = [dataset_utils.get_dir_name_from_path(x) for x in directories_rgb]
    _PREDICTIONS_PATH = cfg['_PREDICTIONS_PATH']
    dataset_utils.copy_directories(source=_RGB_DIRECTORIES_PATH,
                                   target=_PREDICTIONS_PATH)
    directories_predictions = dataset_utils.get_directories_in_directory(_PREDICTIONS_PATH)
    sorted(directories_predictions)
    sorted(directories_flow)
    sorted(directories_rgb)
    print(directories_rgb)
    sorted(directories_predictions)
    sorted(directories_flow)
    sorted(directories_rgb)

    tf.logging.set_verbosity(tf.logging.INFO)
    eval_type = FLAGS.eval_type

    imagenet_pretrained = FLAGS.imagenet_pretrained

    NUM_CLASSES = 400
    if eval_type == 'rgb600':
        NUM_CLASSES = 600

    if eval_type not in ['rgb', 'rgb600', 'flow', 'joint']:
        raise ValueError('Bad `eval_type`, must be one of rgb, rgb600, flow, joint')

    if eval_type == 'rgb600':
        kinetics_classes = [x.strip() for x in open(_LABEL_MAP_PATH_600)]
    else:
        kinetics_classes = [x.strip() for x in open(_LABEL_MAP_PATH)]

    if eval_type in ['rgb', 'rgb600', 'joint']:
        # RGB input has 3 channels.
        rgb_input = tf.placeholder(
            tf.float32,
            shape=(1, _SAMPLE_VIDEO_FRAMES, _IMAGE_SIZE, _IMAGE_SIZE, 3))

        with tf.variable_scope('RGB'):
            rgb_model = i3d.InceptionI3d(
                NUM_CLASSES, spatial_squeeze=True, final_endpoint='Logits')
            rgb_logits, _ = rgb_model(
                rgb_input, is_training=False, dropout_keep_prob=1.0)

        rgb_variable_map = {}
        for variable in tf.global_variables():

            if variable.name.split('/')[0] == 'RGB':
                if eval_type == 'rgb600':
                    rgb_variable_map[variable.name.replace(':0', '')[len('RGB/inception_i3d/'):]] = variable
                else:
                    rgb_variable_map[variable.name.replace(':0', '')] = variable

        rgb_saver = tf.train.Saver(var_list=rgb_variable_map, reshape=True)

    if eval_type in ['flow', 'joint']:
        # Flow input has only 2 channels.
        flow_input = tf.placeholder(
            tf.float32,
            shape=(1, _SAMPLE_VIDEO_FRAMES, _IMAGE_SIZE, _IMAGE_SIZE, 2))
        with tf.variable_scope('Flow'):
            flow_model = i3d.InceptionI3d(
                NUM_CLASSES, spatial_squeeze=True, final_endpoint='Logits')
            flow_logits, _ = flow_model(
                flow_input, is_training=False, dropout_keep_prob=1.0)
        flow_variable_map = {}
        for variable in tf.global_variables():
            if variable.name.split('/')[0] == 'Flow':
                flow_variable_map[variable.name.replace(':0', '')] = variable
        flow_saver = tf.train.Saver(var_list=flow_variable_map, reshape=True)

    if eval_type == 'rgb' or eval_type == 'rgb600':
        model_logits = rgb_logits
    elif eval_type == 'flow':
        model_logits = flow_logits
    else:
        model_logits = rgb_logits + flow_logits
    model_predictions = tf.nn.softmax(model_logits)

    with tf.Session() as sess:
        feed_dict = {}
        if eval_type in ['rgb', 'rgb600', 'joint']:
            if imagenet_pretrained:
                rgb_saver.restore(sess, _CHECKPOINT_PATHS['rgb_imagenet'])
            else:
                rgb_saver.restore(sess, _CHECKPOINT_PATHS[eval_type])
            tf.logging.info('RGB checkpoint restored')

        if eval_type in ['flow', 'joint']:
            if imagenet_pretrained:
                flow_saver.restore(sess, _CHECKPOINT_PATHS['flow_imagenet'])
            else:
                flow_saver.restore(sess, _CHECKPOINT_PATHS['flow'])
            tf.logging.info('Flow checkpoint restored')

        # Here comes the boom
        # rgb_sample = np.load(_SAMPLE_PATHS['rgb'])
        # tf.logging.info('RGB data loaded, shape=%s', str(rgb_sample.shape))
        # feed_dict[rgb_input] = rgb_sample
        #
        # flow_sample = np.load(_SAMPLE_PATHS['flow'])
        # tf.logging.info('Flow data loaded, shape=%s', str(flow_sample.shape))
        # feed_dict[flow_input] = flow_sample
        #
        # out_logits, out_predictions = sess.run(
        #     [model_logits, model_predictions],
        #     feed_dict=feed_dict)
        #
        # out_logits = out_logits[0]
        # out_predictions = out_predictions[0]
        # sorted_indices = np.argsort(out_predictions)[::-1]
        #
        # print('Norm of logits: %f' % np.linalg.norm(out_logits))
        # print('\nTop classes and probabilities')
        # for index in sorted_indices[:20]:
        #     print(out_predictions[index], out_logits[index], kinetics_classes[index])

        ##########################################################################
        ##########################################################################
        # Error: Cannot feed value of shape (220, 240, 320, 3)
        # rgb_sample = np.load(_SAMPLE_PATHS['rgb2'])

        for dir_rgb, dir_flow in zip(directories_flow, directories_rgb):
            print(dir_rgb)
            print(dir_flow)
            samples_rgb_list = dataset_utils.get_files_in_directory(dir_rgb)
            samples_flow_list = dataset_utils.get_files_in_directory(dir_flow)
            for sample_rgb, sample_flow in zip(samples_rgb_list, samples_flow_list):
                sample_name = sample_rgb.replace('.avi', '')
                print(sample_name)
                sample_rgb_full_path = '{}/{}'.format(dir_rgb, sample_rgb)
                sample_flow_full_path = '{}/{}'.format(dir_flow, sample_flow)
                # RGB SAMPLE
                rgb_sample_new = cvrt.scale_numpy_video(sample=np.load(sample_rgb_full_path),
                                                        sample_type='rgb')
                tf.logging.info('RGB data loaded, shape=%s', str(rgb_sample_new.shape))
                feed_dict[rgb_input] = rgb_sample_new

                # FLOW SAMPLE
                flow_sample_new = cvrt.scale_numpy_video(sample=np.load(sample_flow_full_path),
                                                         sample_type='flow')
                tf.logging.info('Flow data loaded, shape=%s', str(flow_sample_new.shape))
                feed_dict[flow_input] = flow_sample_new

                out_logits, out_predictions = sess.run([model_logits, model_predictions], feed_dict=feed_dict)

                out_logits = out_logits[0]
                out_predictions = out_predictions[0]
                sorted_indices = np.argsort(out_predictions)[::-1]

                print('Norm of logits: %f' % np.linalg.norm(out_logits))
                print('\nTop classes and probabilities')
                for index in sorted_indices[:20]:
                    print(out_predictions[index], out_logits[index], kinetics_classes[index])


##########################################################################
##########################################################################


if __name__ == '__main__':
    tf.app.run(main)
