import os
import yaml
import dataset.utils as utl

dir_path = os.path.dirname(os.path.realpath(__file__))

# Read yaml file
with open(dir_path + "/resources/ucf101.yaml") as ymlfile:
    cfg = yaml.safe_load(ymlfile)

video_classes = []
video_classes_names = []
for cls in cfg['classes']:
    if cls in cfg['interest_classes']:
        var = cfg['classes']
        for single_class in var[cls]:
            video_classes.append(cfg['path'] + '/' + single_class)
            video_classes_names.append(single_class)

video_classes_dict = {}
i = 0
for directory in video_classes:
    directory = directory.replace(' ', '')
    # directory = directory.replace('-', '')
    print(directory)
    number = len(utl.get_files_in_directory(directory))
    print(number)
    video_classes_dict[video_classes_names[i]] = number
    i = i + 1

total_videos = 0
for video in video_classes_dict:
    total_videos = total_videos + video_classes_dict[video]
video_classes_dict['total'] = total_videos
print(total_videos)

with open('../output/ucf101_after_processing.yaml', 'w') as outfile:
    yaml.dump(video_classes_dict, outfile, default_flow_style=False)


# For each directory how long are the files
file_name = 'ucf101_video_lengths.yaml'
dataset_directory = cfg['path']

directories = utl.get_directories_in_directory(dataset_directory)
directories.pop(0)
print(directories)

# for all directories get videos' length
stat_dict = {}
frames_dict = {}
for directory in directories:
    print(directory)
    name_length_dict = {}
    frame_tmp_dir = {}
    files = utl.get_files_in_directory(directory)
    for file in files:
        path = directory + '/' + file
        length = utl.get_length(path)
        frames = utl.get_frames(path)
        name_length_dict[file] = length
        frame_tmp_dir[file] = frames
    dir_name = utl.get_dir_name_from_path(directory)
    stat_dict[dir_name] = name_length_dict
    frames_dict[dir_name] = frame_tmp_dir

# write this statistics to file
with open('../output/ucf101_video_lengths.yaml', 'w') as outfile:
    yaml.dump(stat_dict, outfile, default_flow_style=False)

with open('../output/ucf101_video_frames.yaml', 'w') as outfile:
    yaml.dump(frames_dict, outfile, default_flow_style=False)

print('That\'s all, folks!')
