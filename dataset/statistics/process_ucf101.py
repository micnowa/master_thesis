import os
import yaml
import ffmpy
import imageio
import dataset.utils

dir_path = os.path.dirname(os.path.realpath(__file__))


def copy_directories(source, target):
    class_directories = dataset.utils.get_directories_in_directory(source)
    try:
        os.mkdir(target)
    except OSError:
        print('Couldn\'t create directory')
    for class_directoriy in class_directories:
        path = os.path.basename(os.path.normpath(class_directoriy))
        path = target + '/' + path
        last_dir = os.path.basename(os.path.normpath(path))
        if last_dir != 'UCF-101':
            try:
                os.mkdir(path)
            except OSError:
                print('Couldn\'t create directory')


def video_to_gif(video, video_name, path_to_save):
    ff = ffmpy.FFmpeg(
        inputs={"video": None},
        outputs={path_to_save + '/' + video_name: None})


# Read yaml file
with open(dir_path + "/resources/ucf101.yaml") as ymlfile:
    cfg = yaml.safe_load(ymlfile)

dir_to_save = cfg['process_files_dir']

directories = dataset.utils.get_directories_in_directory(cfg['path'])
print(directories)

copy_directories(source=cfg['path'], target=cfg['process_files_dir'])

directories_to_save = []
for x in directories:
    if x == cfg['path']:
        continue
    last_dir = os.path.basename(os.path.normpath(x))
    dirdir = cfg['process_files_dir'] + '/' + last_dir
    directories_to_save.append(dirdir)

i = 0
directories.pop(0)
for directory in directories:
    videos_names = dataset.utils.get_files_in_directory(directory)
    print(videos_names)
    for video in videos_names:
        # Each file save to gif
        video_name = video
        video = directory + '/' + video
        print(video)
        video_to_gif(video, video_name, directories_to_save[i])
    i = i + 1
