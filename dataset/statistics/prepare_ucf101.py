import os
import shutil
import yaml

import dataset.utils

dir_path = os.path.dirname(os.path.realpath(__file__))

# Read yaml file
with open(dir_path + "/../resources/ucf101.yaml") as ymlfile:
    cfg = yaml.safe_load(ymlfile)

dataset_path = cfg['path']
print('Classes of interest:  ')
for record in cfg['interest_classes']:
    print(record)

purged_classes = []
classes = cfg['classes']
for cls in classes:
    if cls not in cfg['interest_classes']:
        purged_classes.append(classes[cls])

# Determine videos to be purged
directories_to_remove = []
for x in purged_classes:
    for y in x:
        str = y.replace(' ', '')
        str = str.replace('-', '')
        directories_to_remove.append(str)

# In working directory remove directories with are not in classes of interest
for name in directories_to_remove:
    directory = dataset_path + '/' + name
    print('Removing: ' + directory)
    try:
        shutil.rmtree(directory)
    except:
        print('The directory does not exist ...')

# For each directory purge  videos that are not withing time limit
min_length = cfg['min_length']
max_length = cfg['max_length']

# Iterate over the directories to be purged
for directory in dataset.utils.get_directories_in_directory(cfg['path']):
    if directory is cfg['path']:
        continue
    files_list = dataset.utils.get_files_in_directory(directory)
    i = 0
    for item in files_list:
        tmp_item = directory + '\\' + item
        files_list[i] = tmp_item
        dataset.utils.remove_video_not_within_limit(tmp_item, min_length, max_length)
        i = i + 1
    print(files_list)
