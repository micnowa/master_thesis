import os
import yaml
import dataset.utils as utils
import dataset.video_processing.convert as cvrt

_rgb = '_rgb.npy'
_flow = '_flow.npy'

# Videos to RGB numpy array
dir_path = os.path.dirname(os.path.realpath(__file__))

with open(dir_path + "/resources/ucf101.yaml") as ymlfile:
    cfg = yaml.safe_load(ymlfile)

unprocessed_dirs = utils.get_directories_in_directory(cfg['path'])
unprocessed_dirs = unprocessed_dirs[1:]

processed_dirs = utils.get_directories_in_directory(cfg['process_files_dir'])
processed_dirs = processed_dirs[1:]

for x, y in zip(unprocessed_dirs, processed_dirs):
    video_files = utils.get_files_in_directory(x)
    for z in video_files:
        file_path = x + '/' + z
        gif_save_path = y + '/' + z.replace('.avi', '')
        npy_save_path = y + '/' + z.replace('.avi', _rgb)
        # Video form AVI to RGB numpy array
        cvrt.convertfile(file_path, gif_save_path, cvrt.TargetFormat.GIF)
        print('File: ', file_path)
        print('Saved as GIF: ', gif_save_path)
    print(video_files)
