import sys
import imageio
import numpy as np


class TargetFormat(object):
    GIF = ".gif"
    MP4 = ".mp4"
    AVI = ".avi"


def convertfile(inputpath, outputpath, targetformat, ):
    reader = imageio.get_reader(inputpath)
    fps = reader.get_meta_data()['fps']

    writer = imageio.get_writer(outputpath + targetformat, fps=fps)
    for i, im in enumerate(reader):
        sys.stdout.write("\rframe {0}".format(i))
        sys.stdout.flush()
        writer.append_data(im)
    print("\r\nFinalizing...")
    writer.close()
    print("Done.")


def scale_numpy_video(sample, sample_type):
    target_frames = 79
    target_width = 224
    target_height = 224
    if sample_type is 'rgb':
        sample_new = np.empty([1, target_frames, target_width, target_height, 3])
    elif sample_type is 'flow':
        sample_new = np.empty([1, target_frames, target_width, target_height, 2])
    else:
        return
    tmp_sample = np.copy(sample)
    shape = tmp_sample.shape
    FRAMES_INDEX = 0
    HEIGHT_INDEX = 1
    WIDTH_INDEX = 2
    CHANNEL_INDEX = 3
    frames = shape[FRAMES_INDEX]
    width = shape[WIDTH_INDEX]
    height = shape[HEIGHT_INDEX]
    channel = shape[CHANNEL_INDEX]
    if sample_type is 'flow':
        tmp_sample = np.delete(tmp_sample, channel-1, CHANNEL_INDEX)
    remove_width = width - target_width
    remove_width_left_right = int(remove_width / 2)
    remove_height = height - target_height
    remove_height_up_down = int(remove_height / 2)
    tmp_sample = np.delete(tmp_sample, slice(target_frames, frames), FRAMES_INDEX)
    tmp_sample = np.delete(tmp_sample, slice(0, remove_width_left_right), WIDTH_INDEX)
    tmp_sample = np.delete(tmp_sample, slice(width - remove_width, width - remove_width_left_right), WIDTH_INDEX)
    tmp_sample = np.delete(tmp_sample, slice(0, remove_height_up_down), HEIGHT_INDEX)
    tmp_sample = np.delete(tmp_sample, slice(height - remove_height, height - remove_height_up_down), HEIGHT_INDEX)

    sample_new[0] = tmp_sample
    return sample_new
