import os

import cv2
import yaml

import dataset.utils as utl
import dataset.video_processing.convert as converter

dir_path = os.path.dirname(os.path.realpath(__file__))

with open(dir_path + "/resources/config.yaml") as ymlfile:
    cfg = yaml.safe_load(ymlfile)

videos_ucf101fail = utl.get_files_in_directory(cfg['UCF-101-fail'])
for video_name in videos_ucf101fail:
    video_path = os.path.join(cfg['UCF-101-fail'], video_name)
    output_video = os.path.join(cfg['UCF-101-fail-avi'], video_name.replace('.mp4', '.avi'))
    converter.convertfile(inputpath=video_path,
                          outputpath=output_video,
                          targetformat=converter.TargetFormat.AVI)
    # Changing size
    # cap = cv2.VideoCapture(output_video)
    # fourcc = cv2.VideoWriter_fourcc(*'XVID')
    # out = cv2.VideoWriter(output_video.replace('UCF-101-fail-avi', 'UCF-101-fail-avi-size') + '.avi', fourcc, 5,
    #                       (320, 240))
    #
    # while True:
    #     ret, frame = cap.read()
    #     if ret == True:
    #         b = cv2.resize(frame, (320, 240), fx=0, fy=0, interpolation=cv2.INTER_CUBIC)
    #         out.write(b)
    #     else:
    #         break
    # cap.release()
    # out.release()
    # cv2.destroyAllWindows()
