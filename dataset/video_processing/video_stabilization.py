# Import numpy and OpenCV
from time import sleep

import numpy as np
import cv2


def moving_average(curve, radius):
    window_size = 2 * radius + 1
    # Define the filter
    f = np.ones(window_size) / window_size
    # Add padding to the boundaries
    curve_pad = np.lib.pad(curve, (radius, radius), 'edge')
    # Apply convolution
    curve_smoothed = np.convolve(curve_pad, f, mode='same')
    # Remove padding
    curve_smoothed = curve_smoothed[radius:-radius]
    # return smoothed curve
    return curve_smoothed


def smooth(trajectory_arg):
    tmp = np.copy(trajectory_arg)
    # Filter the x, y and angle curves
    for k in range(3):
        tmp[:, k] = moving_average(trajectory_arg[:, k], radius=SMOOTHING_RADIUS)
    return tmp


def fix_border(fr):
    s = fr.shape
    # Scale the image 4% without moving the center
    t = cv2.getRotationMatrix2D((s[1] / 2, s[0] / 2), 0, 1.04)
    fr = cv2.warpAffine(fr, t, (s[1], s[0]))
    return fr


SMOOTHING_RADIUS = 2

sample_path = 'C:/Users/micha/Documents/Informatyka_EiTI_2018-2020/mgr/datasets/sample'
sample_avi = 'v_Archery_g01_c01.avi'
in_path = '{}/{}'.format(sample_path, sample_avi)
out_path = '{}/{}'.format(sample_path, 'stabilized2.avi')

# Read input video
cap = cv2.VideoCapture(in_path)

# Get frame count
n_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
fps = int(cap.get(cv2.CAP_PROP_FPS))

# Get width and height of video stream
w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

# Define the codec for output video
fourcc = cv2.VideoWriter_fourcc(*'DIVX')
out = cv2.VideoWriter(out_path, fourcc, fps, (w, h))

# Read first frame
_, prev = cap.read()

# Convert frame to grayscale
prev_gray = cv2.cvtColor(prev, cv2.COLOR_BGR2GRAY)

# Pre-define transformation-store array
transforms = np.zeros((n_frames - 1, 3), np.float32)

for i in range(n_frames - 2):
    # Detect feature points in previous frame
    prev_pts = cv2.goodFeaturesToTrack(prev_gray,
                                       maxCorners=200,
                                       qualityLevel=0.01,
                                       minDistance=50,
                                       blockSize=3)

    # Read next frame
    success, curr = cap.read()
    if not success:
        break

    # Convert to grayscale
    curr_gray = cv2.cvtColor(curr, cv2.COLOR_BGR2GRAY)

    # Calculate optical flow (i.e. track feature points)
    curr_pts, status, err = cv2.calcOpticalFlowPyrLK(prev_gray, curr_gray, prev_pts, None)

    # Sanity check
    assert prev_pts.shape == curr_pts.shape

    # Filter only valid points
    idx = np.where(status == 1)[0]
    prev_pts = prev_pts[idx]
    curr_pts = curr_pts[idx]

    m = cv2.estimateAffine2D(prev_pts, curr_pts)
    # m = cv2.estimateRigidTransform(prev_pts, curr_pts, fullAffine=False)  # will only work with OpenCV-3 or less

    # Extract translation
    m_prim = m[0]
    dx = m_prim[0, 2]
    dy = m_prim[1, 2]

    # Extract rotation angle
    da = np.arctan2(m_prim[1, 0], m_prim[0, 0])

    # Store transformation
    transforms[i] = [dx, dy, da]

    # Move to next frame
    prev_gray = curr_gray

    print("Frame: " + str(i) + "/" + str(n_frames) + " -  Tracked points : " + str(len(prev_pts)))

# Compute trajectory using cumulative sum of transformations
trajectory = np.cumsum(transforms, axis=0)

# Smooth trajectory
smoothed_trajectory = smooth(trajectory)

# Calculate difference in smoothed_trajectory and trajectory
difference = smoothed_trajectory - trajectory

# Calculate newer transformation array
transforms_smooth = transforms + difference

# Reset stream to first frame
cap.set(cv2.CAP_PROP_POS_FRAMES, 0)

# Write n_frames-1 transformed frames
for i in range(n_frames - 2):
    # Read next frame
    success, frame = cap.read()
    if not success:
        break

    # Extract transformations from the new transformation array
    dx = transforms_smooth[i, 0]
    dy = transforms_smooth[i, 1]
    da = transforms_smooth[i, 2]

    # Reconstruct transformation matrix accordingly to new values
    m_prim = np.zeros((2, 3), np.float32)
    m_prim[0, 0] = np.cos(da)
    m_prim[0, 1] = -np.sin(da)
    m_prim[1, 0] = np.sin(da)
    m_prim[1, 1] = np.cos(da)
    m_prim[0, 2] = dx
    m_prim[1, 2] = dy

    # Apply affine wrapping to the given frame
    frame_stabilized = cv2.warpAffine(frame, m_prim, (w, h))

    # Fix border artifacts
    frame_stabilized = fix_border(frame_stabilized)

    # Write the frame to the file
    frame_out = cv2.hconcat([frame, frame_stabilized])

    cv2.imshow("Before and After", frame_out)
    cv2.waitKey(30)
    out.write(frame_out)

cap.release()
out.release()
cv2.destroyAllWindows()
# Set up output video
