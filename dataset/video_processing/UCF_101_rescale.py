import os

import cv2
import yaml

import dataset.utils as utl
import dataset.video_processing.convert as converter

dir_path = os.path.dirname(os.path.realpath(__file__))

with open(dir_path + "/resources/config.yaml") as ymlfile:
    cfg = yaml.safe_load(ymlfile)

videos_ucf101fail = utl.get_files_in_directory(cfg['UCF-101-fail-avi'])
for video_name in videos_ucf101fail:
    video_path = os.path.join(cfg['UCF-101-fail-avi'], video_name)
    output_video = os.path.join(cfg['UCF-101-fail-avi-size'], video_name)
    # ffmpeg -i BalanceBeam_1_1.avi -s 320x240 BalanceBeam_1_1_scaled.avi
    command = 'ffmpeg -i {} -s 320x240 {}'.format(video_path, output_video)
    os.system(command)
