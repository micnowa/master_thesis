import os
import numpy as np
import yaml
from PIL import Image, ImageSequence
from dataset import utils as utl

dir_path = os.path.dirname(os.path.realpath(__file__))


def gif_to_np_grb_array(file_to_read, file_to_save):
    img = Image.open(file_to_read)
    frames = np.array(
        [np.array(frame.copy().convert('RGB').getdata(), dtype=np.uint8).reshape(frame.size[1], frame.size[0], 3) for
         frame
         in ImageSequence.Iterator(img)])
    np.save(file=file_to_save, arr=frames)


# Read config yaml file
with open(dir_path.replace('video_processing', '') + "/resources/ucf101.yaml") as ymlfile:
    cfg = yaml.safe_load(ymlfile)

rgb_numpy_array_dir = cfg['rgb_numpy_array_dir']
print(rgb_numpy_array_dir)

dataset_path = cfg['process_files_dir']
dirs = utl.get_directories_in_directory(dataset_path)

utl.copy_directories(cfg['process_files_dir'], cfg['rgb_numpy_array_dir'])
rgb_dirs = utl.get_directories_in_directory(cfg['rgb_numpy_array_dir'])
utl.copy_directories(cfg['process_files_dir'], cfg['opt_numpy_array_dir'])

# Videos to gifs
for dir, rgb_dir in zip(dirs, rgb_dirs):
    files = utl.get_files_in_directory(dir)
    print(files)
    for file in files:
        video_path = dir + '/' + file
        array_path = rgb_dir + '/' + file.replace('.gif', '')
        print(video_path)
        print(array_path)
        gif_to_np_grb_array(video_path, array_path)
