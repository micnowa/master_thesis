import os
import yaml
import dataset.utils as utils
import dataset.video_processing.convert as cvrt

_rgb = '_rgb.npy'
_flow = '_flow.npy'

# Videos to RGB numpy array
dir_path = os.path.dirname(os.path.realpath(__file__))

with open(dir_path + "/resources/ucf101.yaml") as ymlfile:
    cfg = yaml.safe_load(ymlfile)

directories_full_path = utils.get_directories_in_directory(cfg['path'])
directories_full_path = directories_full_path[1:]

processed_dirs_full_path = utils.get_directories_in_directory(cfg['process_files_dir'])
processed_dirs_full_path = processed_dirs_full_path[1:]

directories = []
for dir in directories_full_path:
    dir.replace('\\\\', '/')
    directories.append(utils.get_dir_name_from_path(dir))

processed_dirs = []
for dir in processed_dirs_full_path:
    processed_dirs.append(utils.get_dir_name_from_path(dir))

dirs_to_save = []
for dir in directories:
    if dir not in processed_dirs:
        dirs_to_save.append(cfg['process_files_dir'] + '/' + dir)
    else:
        # Removing form table dirs
        for x in directories_full_path:
            if dir in x:
                directories_full_path.remove(x)


for x, y in zip(directories_full_path, dirs_to_save):
    video_files = utils.get_files_in_directory(x)
    os.mkdir(y)
    for z in video_files:
        file_path = x + '/' + z
        gif_save_path = y + '/' + z.replace('.avi', '')
        npy_save_path = y + '/' + z.replace('.avi', _rgb)
        # Video form AVI to RGB numpy array
        cvrt.convertfile(file_path, gif_save_path, cvrt.TargetFormat.GIF)
        print('File: ', file_path)
        print('Saved as GIF: ', gif_save_path)
    print(video_files)
