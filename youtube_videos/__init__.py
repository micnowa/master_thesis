from __future__ import unicode_literals
from pytube import YouTube


def download_youtube_video_here(youtube_link, output_path, file_name):
    video = YouTube(youtube_link)
    video.streams.all()
    video.streams.get_by_itag(18).download(output_path=output_path,
                                           filename=file_name)


# def my_hook(d):
#     if d['status'] == 'finished':
#         print('Done downloading, now converting ...')
#
# ydl_opts = {
#     'outtmpl': '%(id)s',
#     'noplaylist' : True,
#     'progress_hooks': [my_hook],
# }
#
# link = input("Youtube videos link:  ")
# with youtube_dl.YoutubeDL(ydl_opts) as ydl:
#     ydl.download([link])

if __name__ == '__main__':
    video_path = 'C:\\Users\\micha\\Documents\\Informatyka_EiTI_2018-2020\\mgr\\datasets\\UCF-101-fail'
    download_youtube_video_here(
        'https://www.youtube.com/watch?v=Rg4HQ1RhhYk&list=RDGMEM2VCIgaiSqOfVzBAjPJm-agVM37XsWvYzUzU&index=2&ab_channel=DaddyYankee',
        video_path, 'tmp_video')
