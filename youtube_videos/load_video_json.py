import os
import yaml
import json
from pytube import YouTube
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip


def download_youtube_video(youtube_link, output_path, file_name):
    video = YouTube(youtube_link)
    video.streams.all()
    video.streams.get_by_itag(18).download(output_path=output_path,
                                           filename=file_name)


def create_new_video(path, file_name, j, video_start_time, video_end_time):
    ffmpeg_extract_subclip(path + '\\' + file_name + '.mp4',
                           t1=video_start_time,
                           t2=video_end_time,
                           targetname=path + file_name + '_' + str(j) + '.mp4')


dir_path = os.path.dirname(os.path.realpath(__file__))
with open(dir_path + "/resources/config.yaml") as ymlfile:
    cfg = yaml.safe_load(ymlfile)
json_file = cfg['videos_json']
print(json_file)
with open(json_file) as infile:
    json_data = json.load(infile)
    print(infile)
print(json_data)

for action in json_data:
    i = 1
    video_dict = json_data[action]
    for entry in video_dict:
        link = entry['link']
        video_file_name = action + '_' + str(i)
        video_path = cfg['output_dir']
        if os.path.isfile('{}{}.mp4'.format(video_path, video_file_name)):
            print('File already exists!')
            i = i + 1
            continue
        print('Saved as {}'.format(video_file_name))
        print(link)
        download_youtube_video(youtube_link=link,
                               output_path=video_path,
                               file_name=video_file_name)
        i = i + 1
        # j = 1
        # for time_frames in entry['time_frames']:
        #     start_time = time_frames['start_time']
        #     end_time = time_frames['end_time']
        #     print(start_time)
        #     print(end_time)
        #     create_new_video(path=video_path,
        #                      file_name=video_file_name,
        #                      j=j,
        #                      video_start_time=start_time,
        #                      video_end_time=end_time)
        #     j = j + 1
        # i = i + 1
