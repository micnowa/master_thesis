import os
import yaml
import pose_detection.pose_utils as pose_utils
import json


def pose_directory_to_one_json(pose_directory):
    keypoints_for_frames = {}
    json_files_paths = pose_utils.get_files_in_directory(pose_directory)
    i = 1
    for json_file_name in json_files_paths:
        json_file_path = '{}/{}'.format(pose_directory, json_file_name)
        with open(json_file_path) as json_file:
            json_data = json.load(json_file)
        people_tab = json_data['people']
        person_keypoints = []
        for person_json in people_tab:
            person_keypoints.append(person_json['pose_keypoints_2d'])
        keypoints_for_frames[str(i)] = person_keypoints
        i = i + 1
    print(keypoints_for_frames)
    return keypoints_for_frames


if __name__ == '__main__':
    print('Pose estimation begin')
    dir_path = os.path.dirname(os.path.realpath(__file__))

    with open(dir_path + "/resources/config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    pose_full_path = pose_utils.get_directories_in_directory(cfg['UCF-101-fail-pose'])
    print(pose_full_path)
    pose_full_path = sorted(pose_full_path)
    print(pose_full_path)

    execution_path = os.getcwd()

    # ACTION (videos in directory)
    for action_directory in pose_full_path:
        json_data = pose_directory_to_one_json(action_directory)
        with open('{}{}'.format(action_directory.replace('UCF-101-fail-pose', 'UCF-101-fail-pose-json'), '.json'),
                  'w') as outfile:
            json.dump(json_data, outfile)
