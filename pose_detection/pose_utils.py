import ntpath
import os
from os import listdir
from os.path import isfile, join
import cv2


def get_frames(video_name):
    cap = cv2.VideoCapture(video_name)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    return frame_count


def get_length(video_name):
    cap = cv2.VideoCapture(video_name)
    fps = cap.get(cv2.CAP_PROP_FPS)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    return frame_count / fps


def within_length(path, minimum, maximum):
    return minimum <= get_length(path) <= maximum


def get_files_in_directory(path):
    only_files = [f for f in listdir(path) if isfile(join(path, f))]
    return only_files


def get_directories_in_directory(path):
    var = [dirr[0] for dirr in os.walk(path)]
    var.pop(0)
    return var


def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


def remove_video_not_within_limit(video, minimum, maximum):
    within_limit = within_length(video, minimum, maximum)
    if within_limit is False:
        os.remove(video)


def get_dir_name_from_path(path):
    return os.path.basename(os.path.normpath(path))


def copy_directories(source, target):
    class_directories = get_directories_in_directory(source)
    try:
        os.mkdir(target)
    except OSError:
        print('Couldn\'t create directory')
    for class_directoriy in class_directories:
        path = os.path.basename(os.path.normpath(class_directoriy))
        path = target + '/' + path
        last_dir = os.path.basename(os.path.normpath(path))
        if last_dir != 'UCF-101':
            try:
                os.mkdir(path)
            except OSError:
                print('Couldn\'t create directory')
