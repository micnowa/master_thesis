import os
import unittest
import json
from pose_detection.openpose_dir_to_json import pose_directory_to_one_json

dir_path = os.path.dirname(os.path.realpath(__file__))
pose_path = 'resources/v_CricketShot_g01_c01'


class Testing(unittest.TestCase):
    def test_writes_json(self):
        try:
            print()
            json_dict = pose_directory_to_one_json(os.path.join(dir_path, pose_path))
            with open('resources/data.json', 'w') as outfile:
                json.dump(json_dict, outfile)
        except Exception:
            self.fail('Not valid JSON')


if __name__ == '__main__':
    unittest.main()
