import os
from unittest import TestCase

import numpy as np

from pose_detection.pose_changes_for_frames import pose_changes
from pose_detection.pose_changes_for_frames import translate_coordinates
from pose_detection.pose_changes_for_frames import difference_vector
from pose_detection.pose_changes_for_frames import diff_vector_norm

dir_path = os.path.dirname(os.path.realpath(__file__))
pose_path = 'resources/data.json'


class Test(TestCase):
    prev = np.array([0.3, 0.8, 0.99, 0, 0, 0, 0.5, 0.5, 1, 0.10, 0.90, 0.764])
    curr = np.array([0.7, 0.8, 0.99, 0, 0, 0, 0.5, 0.5, 1, 0.70, 0.30, 0.764])
    diff = np.array([0.4, 0, np.nan, np.nan, 0, 0, 0.60, -0.60])

    def test_pose_changes(self):
        pose_changes('resources/v_CricketShot_g01_c01.json')

    def test_scaling_for_pose(self):
        init_pose = [0.3, 0.8, 0.99, 0, 0, 0, 0.5, 0.5, 1, 0.001, 0.999, 0.764]
        pose_scaled = [0.7, 0.2, 0.99, 0, 0, 0, 0.5, 0.5, 1, 0.999, 0.001, 0.764]
        translate_coordinates(init_pose)
        self.assertEquals(init_pose, pose_scaled)

    def test_difference_vector(self):
        test_diff = difference_vector(prev=self.prev, curr=self.curr)
        np.testing.assert_array_almost_equal(self.diff, test_diff)

    def test_norm_difference_vector(self):
        diff_norm = diff_vector_norm(self.diff)
        test_diff_norm = [np.sqrt(0.16 + 0.36 + 0.36), 2]
        np.testing.assert_array_almost_equal(diff_norm, test_diff_norm)

