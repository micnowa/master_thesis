import os
import yaml
import pose_detection.pose_utils as pose_utils

OPENPOSE_COMMAND = './opt/openpose/openpose/build/examples/openpose/openpose.bin'
VIDEO_OPTION = '--video'
MODELS_OPTION = '--model_folder /opt/openpose/openpose/models'
WRITE_JSON_OPTION = '--write_json'
REMAINING_OPTIONS = '--display 0 --render_pose 0'


def openpose_command(video_path, json_directory):
    video_option = '{} {}'.format(VIDEO_OPTION, video_path)
    dir_option = '{} {}'.format(WRITE_JSON_OPTION, json_directory)
    return '{} {} {} {} {}'.format(OPENPOSE_COMMAND,
                                   video_option,
                                   dir_option,
                                   MODELS_OPTION,
                                   REMAINING_OPTIONS)


if __name__ == '__main__':
    print('Pose estimation begin')
    dir_path = os.path.dirname(os.path.realpath(__file__))

    with open(dir_path + "/resources/config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    print(cfg['UCF-101-fail-avi-size'])
    videos_names = pose_utils.get_files_in_directory(cfg['UCF-101-fail-avi-size'])
    videos_full_path = []
    output_pose_directories = []
    video_path_and_output_dirs = {}
    for video_name in videos_names:
        video_path = os.path.join(cfg['UCF-101-fail-avi-size'], video_name)
        videos_full_path.append(video_path)
        output_dir = os.path.join(cfg['UCF-101-fail-pose'], video_name.replace('.avi', ''))
        output_pose_directories.append(output_dir)
        video_path_and_output_dirs[video_path] = output_dir
    for output_dir in output_pose_directories:
        try:
            os.mkdir(output_dir)
        except:
            print('Directory already exists ...')

    for entry in video_path_and_output_dirs:
        input_video = entry
        print('Input video')
        print(input_video)
        output_pose_dir = video_path_and_output_dirs[input_video]
        print('Output dir:')
        print(output_pose_dir)
        command = openpose_command(input_video, output_pose_dir)
        print(command)
        os.system('cd /opt/openpose/openpose/build/examples/openpose/')
        os.system(command)
