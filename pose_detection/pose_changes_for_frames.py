import json
import os

import numpy as np
import yaml

from pose_detection import pose_utils

KEY_POINT_NUMBER = 25
X_POSITION = 0
Y_POSITION = 1
C_POSITION = 2
POINTS_GROUP = 3
WIDTH = 1
HEIGHT = 1
NORM_INDEX = 0
MISSING_INDEX = 1


def pose_changes(json_file_path):
    poses_for_frames = preprocess_data(json_file_path)
    prev = poses_for_frames[str(1)]
    differences_vectors = {}
    for i in range(2, len(poses_for_frames)):
        frame = str(i)
        differences_vectors[frame] = []
        for poses in poses_for_frames[frame]:
            diff_vector = determine_prev_pose(curr=poses,
                                              prev_poses=prev)
            differences_vectors[frame].append(diff_vector)
    print('New things comming')
    return differences_vectors


def preprocess_data(json_file_path):
    with open(json_file_path) as json_file_path:
        json_data = json.load(json_file_path)
    translate_all_coordinates(json_data)
    return json_data


def difference_vector(curr, prev):
    diff_vector = np.array([])
    for i in range(0, len(curr), POINTS_GROUP):
        if np.isclose(curr[i + C_POSITION], 0) or np.isclose(prev[i + C_POSITION], 0):
            diff_vector = np.append(diff_vector, np.repeat(np.nan, 2))
            continue
        diff_vector = np.append(diff_vector, curr[i + X_POSITION] - prev[i + X_POSITION])
        diff_vector = np.append(diff_vector, curr[i + Y_POSITION] - prev[i + Y_POSITION])
    return diff_vector


def translate_all_coordinates(poses_tab):
    for frame_num in poses_tab:
        for pose in poses_tab[frame_num]:
            translate_coordinates(pose)


def translate_coordinates(pose):
    for i in range(0, len(pose), POINTS_GROUP):
        if pose[i + C_POSITION] is 0:
            continue
        pose[i + X_POSITION] = WIDTH - pose[i + X_POSITION]
        pose[i + Y_POSITION] = HEIGHT - pose[i + Y_POSITION]


def diff_vector_norm(diff_vector):
    missing_values = 0
    norm = 0
    for coordinate in diff_vector:
        if np.isnan(coordinate):
            missing_values = missing_values + 1
            continue
        norm += coordinate * coordinate
    norm = np.sqrt(norm)
    return [norm, missing_values]


def determine_prev_pose(curr, prev_poses):
    diff_v = []
    diff_vn = []
    for pose in prev_poses:
        diff = difference_vector(curr=curr, prev=pose)
        diff_v.append(diff)
        diff_vn.append(diff_vector_norm(diff))
    diff_vn = sorted(diff_vn, key=lambda tup: tup[NORM_INDEX], reverse=True)
    first = diff_vn[0]
    min_miss_val = first[MISSING_INDEX]
    diff_vn = [item for item in diff_vn if item[MISSING_INDEX] == min_miss_val]
    return min(diff_vn, key=lambda tup: tup[NORM_INDEX])


if __name__ == '__main__':
    print('Pose estimation begin')
    dir_path = os.path.dirname(os.path.realpath(__file__))

    with open(dir_path + "/resources/config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    pose_full_path = pose_utils.get_directories_in_directory(cfg['UCF-101-pose-win'])
    v = sorted(pose_full_path)
    actions = [pose_utils.get_dir_name_from_path(item) for item in pose_full_path]
    pose_diff_full_path = [item.replace('UCF-101-pose', 'UCF-101-pose-diff') for item in pose_full_path]
    for dir in pose_diff_full_path:
        try:
            os.mkdir(dir)
        except Exception:
            print('Already exists')
    pose_files = {}
    for dir in pose_full_path:
        action = pose_utils.get_dir_name_from_path(dir)
        files = pose_utils.get_files_in_directory(dir)
        files = [item for item in files if ".json" in item]
        pose_files[dir] = files
        files_full_path = ['{}/{}'.format(dir, item) for item in files]
        for file in files_full_path:
            diff_file = file.replace('UCF-101-pose', 'UCF-101-pose-diff')
            diff_file = diff_file.replace('.json', '-diff.json')
            print(file)
            print(diff_file)
            differences = pose_changes(file)
            with open(diff_file, 'w') as outfile:
                json.dump(differences, outfile)
            print('Saved')
