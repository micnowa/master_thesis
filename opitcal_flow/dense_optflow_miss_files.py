import os

import cv2 as cv
import imageio
import numpy as np
import yaml
from dataset import utils as utl


def video_to_optical_flow(video_path, directory, video_name):
    try:
        frame_num = utl.get_frames(video_path)
        cap = cv.VideoCapture(cv.samples.findFile(video_path))
        ret, frame1 = cap.read()
        prvs = cv.cvtColor(frame1, cv.COLOR_BGR2GRAY)
        hsv = np.zeros_like(frame1)
        hsv[..., 1] = 255
        i = 0
        frames_list = []
        while i < frame_num - 4:
            ret, frame2 = cap.read()
            next = cv.cvtColor(frame2, cv.COLOR_BGR2GRAY)
            flow = cv.calcOpticalFlowFarneback(prvs, next, None, 0.5, 3, 15, 3, 5, 1.2, 0)
            mag, ang = cv.cartToPolar(flow[..., 0], flow[..., 1])
            hsv[..., 0] = ang * 180 / np.pi / 2
            hsv[..., 2] = cv.normalize(mag, None, 0, 255, cv.NORM_MINMAX)
            bgr = cv.cvtColor(hsv, cv.COLOR_HSV2BGR)
            # cv.imshow('frame2', bgr)
            k = cv.waitKey(30) & 0xff
            if k == 27:
                break
            elif k == ord('s'):
                cv.imwrite('opticalfb.png', frame2)
                cv.imwrite('opticalhsv.png', bgr)
            frames_list.append(bgr)
            prvs = next
            i = i + 1
        gif_output = os.path.join(directory, '{}.gif'.format(video_name))
        np_output = os.path.join(directory, '{}.npy'.format(video_name))
        print(gif_output)
        print(np_output)
        imageio.mimsave(gif_output, frames_list)
        np.save(np_output, frames_list)
    except Exception as exp:
        print(exp)


# dir_path = os.path.dirname(os.path.realpath(__file__))
#
# with open(dir_path + "/resources/ucf101.yaml") as ymlfile:
#     cfg = yaml.safe_load(ymlfile)
#
# directories_full_path = utl.get_directories_in_directory(cfg['path'])
# directories_full_path = directories_full_path[1:]
#
# opt_dirs_full_path = []
# for dir in directories_full_path:
#     tmp = dir
#     opt_dirs_full_path.append(tmp.replace('UCF-101', 'UCF-101-opt'))
#
# sorted(directories_full_path)
# sorted(opt_dirs_full_path)
# print(directories_full_path)
# print(opt_dirs_full_path)
#
# for dir_source, dir_dest in zip(directories_full_path, opt_dirs_full_path):
#     video_files = utl.get_files_in_directory(dir_source)
#     if os.path.exists(dir_dest):
#         continue
#     os.mkdir(dir_dest)
#     print(video_files)
#     for video_name in video_files:
#         file_path = dir_source + '/' + video_name
#         print(file_path)
#         print(dir_dest + '/' + video_name.replace('.avi', ''))
#         video_to_optical_flow(file_path, dir_dest, video_name.replace('.avi', ''))


dir_path = os.path.dirname(os.path.realpath(__file__))

with open(dir_path + "/resources/config.yaml") as ymlfile:
    cfg = yaml.safe_load(ymlfile)

videos_ucf101fail = utl.get_files_in_directory(cfg['UCF-101-fail-avi-size'])
for video_name in videos_ucf101fail:
    video_path = os.path.join(cfg['UCF-101-fail-avi-size'], video_name)
    output_video = os.path.join(cfg['UCF-101-fail-avi-size'], video_name.replace('.mp4', '.avi'))
    video_to_optical_flow(video_path=video_path,
                          directory=cfg['UCF-101-fail-opt'],
                          video_name=video_name.replace('.avi', ''))
