import cv2
import numpy as np

sample_path = 'C:/Users/micha/Documents/Informatyka_EiTI_2018-2020/mgr/datasets/sample'
sample_avi = 'v_HulaHoop_g12_c01.avi'
sample_full_avi = sample_path + '/' + sample_avi
sample_gif = 'v_Archery_g01_c01.gif'
sample_full_gif = sample_path + '/' + sample_gif

cap = cv2.VideoCapture(sample_full_avi)

ret, frame1 = cap.read()
prvs = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)
hsv = np.zeros_like(frame1)
hsv[..., 1] = 255

while (1):
    ret, frame2 = cap.read()
    next = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)

    # pyr_scale, levels, winsize, iterations, poly_n, poly_sigma, flags)
    flow = cv2.calcOpticalFlowFarneback(prev=prvs, next=next, flow=None, pyr_scale=0.5, levels=5, winsize=10,
                                        iterations=3, poly_n=8, poly_sigma=5, flags=0)

    mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1])
    hsv[..., 0] = ang * 180 / np.pi / 2
    hsv[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
    rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

    cv2.imshow('frame2', rgb)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
    elif k == ord('s'):
        cv2.imwrite('opticalfb.png', frame2)
        cv2.imwrite('opticalhsv.png', rgb)
    prvs = next

cap.release()
cv2.destroyAllWindows()
