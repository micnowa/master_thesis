import os
import cv2
import imageio
import numpy as np
import yaml
import dataset.utils as utl


def lk_optical_flow(video_path, directory, video_name):
    try:
        cap = cv2.VideoCapture(video_path)
        frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        # params for ShiTomasi corner detection
        feature_params = dict(maxCorners=100,
                              qualityLevel=0.3,
                              minDistance=7,
                              blockSize=7)
        # Parameters for lucas kanade optical flow
        lk_params = dict(winSize=(15, 15),
                         maxLevel=2,
                         criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
        # Create some random colors
        color = np.random.randint(0, 255, (100, 3))
        # Take first frame and find corners in it
        ret, old_frame = cap.read()
        old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
        p0 = cv2.goodFeaturesToTrack(old_gray, mask=None, **feature_params)
        # Create a mask image for drawing purposes
        mask = np.zeros_like(old_frame)
        frames_list = []
        j = 1
        while j < frame_count - 4:
            ret, frame = cap.read()
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            p1, st, err = cv2.calcOpticalFlowPyrLK(prevImg=old_gray,
                                                   nextImg=frame_gray,
                                                   prevPts=p0,
                                                   nextPts=None,
                                                   **lk_params)
            good_new = p1[st == 1]
            good_old = p0[st == 1]
            for i, (new, old) in enumerate(zip(good_new, good_old)):
                a, b = new.ravel()
                c, d = old.ravel()
                mask = cv2.line(mask, (a, b), (c, d), color[i].tolist(), 2)
                frame = cv2.circle(frame, (a, b), 5, color[i].tolist(), -1)
            backtorgb = cv2.cvtColor(frame_gray, cv2.COLOR_GRAY2RGB)
            black_background = np.zeros_like(backtorgb)
            opt_flow_black_background = cv2.add(black_background, mask)
            frames_list.append(opt_flow_black_background)
            k = cv2.waitKey(30) & 0xff
            j = j + 1
            if k == 27:
                break
            old_gray = frame_gray.copy()
            p0 = good_new.reshape(-1, 1, 2)
        # directory = directory.replace('\\', '/')
        gif_path = os.path.join(directory, '{}.gif'.format(video_name))
        np_path = os.path.join(directory, '{}.npy'.format(video_name))
        print(gif_path)
        print(np_path)
        imageio.mimsave(gif_path, frames_list)
        np.save(np_path, frames_list)
    except Exception as exp:
        print(exp)


dir_path = os.path.dirname(os.path.realpath(__file__))

with open(dir_path + "/resources/config.yaml") as ymlfile:
    cfg = yaml.safe_load(ymlfile)

videos_ucf101fail = utl.get_files_in_directory(cfg['UCF-101-fail-avi-size'])
for video_name in videos_ucf101fail:
    video_path = os.path.join(cfg['UCF-101-fail-avi-size'], video_name)
    lk_optical_flow(video_path=video_path,
                    directory=cfg['UCF-101-fail-lk'],
                    video_name=video_name.replace('.avi', ''))
