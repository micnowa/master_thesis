import unittest
import numpy as np

RED_POSITION = 0
GREEN_POSITION = 1
BLUE_POSITION = 2


class Testing(unittest.TestCase):
    def test_writes_json(self):
        numpy_array = np.load(file='resources/v_BalanceBeam_g01_c01.npy')
        numpy_array_red = numpy_array[:, :, :, RED_POSITION]
        numpy_array_green = numpy_array[:, :, :, GREEN_POSITION]
        numpy_array_blue = numpy_array[:, :, :, BLUE_POSITION]
        for frame_red in numpy_array_red:
            print(frame_red)
        for frame_green in numpy_array_green:
            print(frame_green)
        for frame_blue in numpy_array_blue:
            print(frame_blue)
        print(numpy_array)


if __name__ == '__main__':
    unittest.main()
