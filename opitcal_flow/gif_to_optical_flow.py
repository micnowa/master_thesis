# https://docs.opencv.org/trunk/d4/dee/tutorial_optical_flow.html
import os

import cv2 as cv
import imageio
import numpy as np
import yaml

import dataset.utils as utl


# Flownet or PWC flow, (Pytorch)
# cv.optflow_DualTVL1OpticalFlow

# https://www.learnopencv.com/applications-of-foreground-background-separation-with-semantic-segmentation/
# https://docs.opencv.org/3.4/d1/dc5/tutorial_background_subtraction.html
# https://github.com/NVIDIA/flownet2-pytorch
def video_to_optical_flow(video_path, directory, video_name):
    frame_num = utl.get_frames(video_path)
    cap = cv.VideoCapture(cv.samples.findFile(video_path))
    ret, frame1 = cap.read()
    prvs = cv.cvtColor(frame1, cv.COLOR_BGR2GRAY)
    hsv = np.zeros_like(frame1)
    hsv[..., 1] = 255
    i = 0
    frames_list = []
    while i < frame_num - 1:
        ret, frame2 = cap.read()
        next = cv.cvtColor(frame2, cv.COLOR_BGR2GRAY)
        flow = cv.calcOpticalFlowFarneback(prvs, next, flow=None, pyr_scale=0.5, levels=3, winsize=15, iterations=3,
                                           poly_n=5, poly_sigma=1.2, flags=0)
        mag, ang = cv.cartToPolar(flow[..., 0], flow[..., 1])
        hsv[..., 0] = ang * 180 / np.pi / 2
        hsv[..., 2] = cv.normalize(mag, None, 0, 255, cv.NORM_MINMAX)
        bgr = cv.cvtColor(hsv, cv.COLOR_HSV2BGR)
        # cv.imshow('frame2', bgr)
        k = cv.waitKey(30) & 0xff
        if k == 27:
            break
        elif k == ord('s'):
            cv.imwrite('opticalfb.png', frame2)
            cv.imwrite('opticalhsv.png', bgr)
        frames_list.append(bgr)
        prvs = next
        i = i + 1
    imageio.mimsave(directory + '/' + video_name + '.gif', frames_list)
    np.save(directory + '/' + video_name + '.npy', frames_list)


dir_path = os.path.dirname(os.path.realpath(__file__))

with open(dir_path.replace('video_processing', '') + 'resources/ucf101.yaml') as ymlfile:
    cfg = yaml.safe_load(ymlfile)

dir_to_save = cfg['opt_gif_dir']
directories = utl.get_directories_in_directory(cfg['path'])

directories_to_save = []
for x in directories:
    if x == cfg['path']:
        continue
    last_dir = os.path.basename(os.path.normpath(x))
    dirdir = cfg['opt_numpy_array_dir'] + '/' + last_dir
    directories_to_save.append(dirdir)

i = 0
directories.pop(0)
for directory in directories:
    videos_names = utl.get_files_in_directory(directory)
    print(videos_names)
    k = 0
    for video in videos_names:
        if k > 0:
            break
        # Each file save to gif
        video_name = video
        video = directory + '/' + video
        print(video)
        video_to_optical_flow(video, directories_to_save[i], video_name.replace('.avi', ''))
        k = k + 1
    i = i + 1
