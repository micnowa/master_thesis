import os
import pickle
import json
import sys
from os import listdir
from os.path import isfile, join

import numpy as np
from PIL import Image
from imageai.Detection import ObjectDetection, VideoObjectDetection
import yaml
import dataset.utils as dataset_utils

objects_dict = {}
width = 320
height = 240


def for_frame(frame_number, output_array, output_count):
    print("FOR FRAME ", frame_number)
    print("Output for each object : ", output_array)
    print("Output count for unique objects : ", output_count)
    scaled_output_array = []
    for obj in output_array:
        scaled_box_points = obj['box_points']
        box_points_list = []
        i = 0
        for box_point in scaled_box_points:
            if i % 2 is 0:
                box_points_list.append(box_point / width)
            else:
                box_points_list.append(box_point / height)
            i = i + 1
        obj['box_points'] = box_points_list
        scaled_output_array.append(obj)

    objects_dict[frame_number] = scaled_output_array
    print("------------END OF A FRAME --------------")


def all_npy_array_to_obj_flow():
    with open('resources/config.yaml') as yamlfile:
        config = yaml.load(yamlfile)
    npy_videos_path = config['npy_videos_path']
    obj_target_path = config['obj_target_path']
    npy_directories = dataset_utils.get_directories_in_directory(npy_videos_path)
    directories_names = []
    for x in npy_directories:
        directories_names.append(dataset_utils.get_dir_name_from_path(x))
    object_target_directories = []
    for x in directories_names:
        object_target_directories.append(obj_target_path + '/' + x)
    npy_files = {}
    object_files = {}
    for x in npy_directories:
        tmp_list = dataset_utils.get_files_in_directory(x)
        new_list = []
        for y in tmp_list:
            new_list.append(x + '/' + y)
        video_category = dataset_utils.get_dir_name_from_path(x)
        npy_files[video_category] = new_list
        object_files[video_category] = []

    # Load model for detections
    detector = load_detector_model()

    for directory in npy_files:
        for npy_file in npy_files[directory]:
            object_path = obj_target_path + '/' + directory
            object_name = dataset_utils.path_leaf(npy_file)
            object_name = object_name.replace('.avi', '')
            object_name = object_name.replace('.npy', '')
            npy_file_to_object_flow(npy_file=npy_file,
                                    target_path=object_path,
                                    target_name=object_name,
                                    detector=detector)


def npy_file_to_object_flow(npy_file, target_path, target_name, detector):
    print(npy_file)
    output_path = '{}/{}'.format(target_path, target_name)
    print(output_path)
    frames = np.load(npy_file)
    detections = detect_objects(detector_model=detector,
                                frames=frames)
    object_flow = determine_object_flow(detections)
    a_file = open(output_path.join('.pkl'), "wb")
    pickle.dump(object_flow, a_file)
    a_file.close()


def load_detector_model():
    execution_path = os.getcwd()
    detector = ObjectDetection()
    detector.setModelTypeAsRetinaNet()
    detector.setModelPath(os.path.join(execution_path, "resources/resnet50_coco_best_v2.0.1.h5"))
    detector.loadModel()
    return detector


def detect_objects(detector_model, frames):
    detections_list = []
    for i in range(0, frames.shape[0]):
        current_frame = frames[i]
        img = Image.fromarray(current_frame, 'RGB')
        new_img = img.convert('RGB')
        detections = detector_model.detectObjectsFromImage(input_image=new_img,
                                                           input_type='array',
                                                           extract_detected_objects=False,
                                                           output_image_path=None,
                                                           minimum_percentage_probability=50,
                                                           display_percentage_probability=True,
                                                           display_object_name=True,
                                                           thread_safe=False)
        detections_list.append(detections)
        for eachObject in detections:
            print(eachObject["name"], " : ", eachObject["percentage_probability"])
    # Scale frames
    height = frames.shape[1]
    width = frames.shape[2]
    simplified_detections = []
    for each_detections in detections_list:
        tmp_detection = []
        for each_detection in each_detections:
            box_points = each_detection['box_points']
            box_points[0] = box_points[0] / width
            box_points[2] = box_points[2] / width
            box_points[1] = box_points[1] / height
            box_points[3] = box_points[2] / height
            # each_detection['box_points'] = box_points
            name = each_detection['name']
            new_detection = {'box_points': box_points, 'name': name}
            tmp_detection.append(new_detection)
        simplified_detections.append(tmp_detection)
    return simplified_detections


def determine_object_flow(detections):
    objects = {}
    # Get all objects from detected ones
    for list in detections:
        for dictionary in list:
            name = dictionary['name']
            if name not in object:
                objects[name] = {}

    # for each object determine flow
    for obj in objects:
        dict = {}
        i = 0
        for list in detections:
            for dictionary in list:
                if dictionary['name'] == obj:
                    box_points = dictionary['box_points']
                    x = 0.5 * (box_points[0] + box_points[2])
                    y = 0.5 * (box_points[1] + box_points[3])
                    dict[i] = (x, y)
            i = i + 1
        objects[obj] = dict
    return objects


def get_files_in_directory(path):
    only_files = [f for f in listdir(path) if isfile(join(path, f))]
    return only_files


if __name__ == '__main__':
    dir_path = os.path.dirname(os.path.realpath(__file__))

    with open(dir_path + "/resources/config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    with open(dir_path + "/resources/missing_actions.json") as jsonfile:
        json_data = json.load(jsonfile)

    UCF_path = cfg['UCF-101']
    obj_target_path = cfg['obj_target_path']
    missing_actions = json_data['missing']
    videos_full_path = {}
    videos_names_for_actions = {}
    missing_dirs = [os.path.join(UCF_path, item) for item in missing_actions]

    for action in missing_actions:
        full_dir_path = ''
        for directory in missing_dirs:
            if action in directory:
                full_dir_path = directory
                continue
        videos_names = get_files_in_directory(full_dir_path)
        videos_names_for_actions[action] = videos_names
        videos_full_path[action] = [os.path.join(full_dir_path, item) for item in videos_names]

    for action in videos_names_for_actions:
        videos_names = videos_names_for_actions[action]
        new_video_files = []
        new_video_files_full_path = []
        for i in range(0, len(videos_names)):
            if i < 10:
                substring = '_g0{}'.format(str(i))
            else:
                substring = '_g{}'.format(str(i))
            videos_meeting_criteria = [item for item in videos_names if substring in item]
            if videos_meeting_criteria:
                target_video = videos_meeting_criteria[0]
                for full_video_path in videos_full_path[action]:
                    if target_video in full_video_path:
                        new_video_files_full_path.append(full_video_path)
                new_video_files.append(target_video)
        videos_names_for_actions[action] = new_video_files
        videos_full_path[action] = new_video_files_full_path

    execution_path = os.getcwd()
    video_detector = VideoObjectDetection()
    video_detector.setModelTypeAsRetinaNet()
    video_detector.setModelPath(os.path.join(execution_path, "resources/resnet50_coco_best_v2.0.1.h5"))
    video_detector.loadModel()

    for action, duplicate in zip(videos_names_for_actions, videos_full_path):
        target_dir = os.path.join(obj_target_path, action)
        try:
            os.mkdir(target_dir)
        except Exception:
            print('Already exists')
        videos = videos_names_for_actions[action]
        full_path_video = videos_full_path[action]
        for video, full_path_video in zip(videos, full_path_video):
            # print(video)
            print(full_path_video)
            name_to_save = os.path.join(target_dir, video)
            print(name_to_save)
            output_object_dict = os.path.join(target_dir, video.replace('.avi', ''))
            objects_dict = {}
            detections = video_detector.detectObjectsFromVideo(
                input_file_path=full_path_video,
                frames_per_second=25,
                output_file_path=output_object_dict,
                display_object_name=True,
                display_percentage_probability=True,
                log_progress=True,
                per_frame_function=for_frame
            )
            print(objects_dict)
            json_output_object_dict = '{}.json'.format(output_object_dict)
            json = json.dumps(objects_dict)
            f = open(json_output_object_dict, "w")
            f.write(json)
            f.close()
