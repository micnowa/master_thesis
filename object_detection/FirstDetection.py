from imageai.Detection import ObjectDetection, VideoObjectDetection
import os
from PIL import Image
import numpy as np
import os

import numpy as np
from PIL import Image
from imageai.Detection import ObjectDetection

# with open('resources/config.yaml') as yamlfile:
#     config = yaml.load(yamlfile)
#
# npy_videos_path = config['npy_videos_path']
# obj_target_path = config['obj_target_path']
#
# npy_directories = dataset_utils.get_directories_in_directory(npy_videos_path)
# directories_names = []
# for x in npy_directories:
#     directories_names.append(dataset_utils.get_dir_name_from_path(x))
# object_target_directories = []
# for x in directories_names:
#     object_target_directories.append(obj_target_path + '/' + x)
#
# print(object_target_directories)
#
# npy_files = {}
# object_files = {}
# for x in npy_directories:
#     tmp_list = dataset_utils.get_files_in_directory(x)
#     new_list = []
#     for y in tmp_list:
#         new_list.append(x + '/' + y)
#     video_category = dataset_utils.get_dir_name_from_path(x)
#     npy_files[video_category] = new_list
#     object_files[video_category] = []
#
# for directory in npy_files:
#     for npy_file in npy_files[directory]:
#         print(npy_file)

# frames = np.load('C:/Users/micha/Documents/Informatyka_EiTI_2018-2020/mgr/datasets/sample/v_BalanceBeam_g01_c01.npy')

objects_dict = {}


def forFrame(frame_number, output_array, output_count):
    print("FOR FRAME " , frame_number)
    print("Output for each object : ", output_array)
    print("Output count for unique objects : ", output_count)
    objects_dict[frame_number] = output_array
    print("------------END OF A FRAME --------------")


execution_path = os.getcwd()

# detector = ObjectDetection()
# detector.setModelTypeAsRetinaNet()
# detector.setModelPath(os.path.join(execution_path, "resources/resnet50_coco_best_v2.0.1.h5"))
# detector.loadModel()

video_detector = VideoObjectDetection()
video_detector.setModelTypeAsRetinaNet()
video_detector.setModelPath(os.path.join(execution_path, "resources/resnet50_coco_best_v2.0.1.h5"))
video_detector.loadModel()

detections_list = []

detections = video_detector.detectObjectsFromVideo(
    input_file_path="C:/Users/micha/Documents/Informatyka_EiTI_2018-2020/mgr/datasets/sample/v_Archery_g01_c01.avi",
    frames_per_second=25,
    output_file_path='firstDetection',
    display_object_name=True,
    display_percentage_probability=True,
    log_progress=True,
    per_frame_function=forFrame
    )
print(detections)
# for i in range(0, frames.shape[0]):
#     # for each frame
#     curr = frames[i]
#     img = Image.fromarray(curr, 'RGB')
#     new_img = img.convert('RGB')
#     detections = detector.detectObjectsFromImage(input_image=new_img,
#                                                  input_type='array',
#                                                  output_type=None,
#                                                  output_image_path="",
#                                                  extract_detected_objects=False,
#                                                  minimum_percentage_probability=50,
#                                                  display_percentage_probability=True,
#                                                  display_object_name=True,
#                                                  thread_safe=False)
#     detections_list.append(detections)
#     for eachObject in detections:
#         print(eachObject["name"], " : ", eachObject["percentage_probability"])
#
# print('Whatsoever')
# height = frames.shape[1]
# width = frames.shape[2]
#
# simplified_detections = []
#
# for each_detections in detections_list:
#     tmp_detection = []
#     for each_detection in each_detections:
#         box_points = each_detection['box_points']
#         box_points[0] = box_points[0] / width
#         box_points[2] = box_points[2] / width
#         box_points[1] = box_points[1] / height
#         box_points[3] = box_points[2] / height
#         # each_detection['box_points'] = box_points
#         name = each_detection['name']
#         new_detection = {'box_points': box_points, 'name': name}
#         tmp_detection.append(new_detection)
#     simplified_detections.append(tmp_detection)

print('Script finished ...')
