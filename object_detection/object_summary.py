import os
import yaml
import dataset.utils as dataset_utils

if __name__ == '__main__':
    print('Object summary in place')
    dir_path = os.path.dirname(os.path.realpath(__file__))

    with open(dir_path + "/resources/config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    actions = dataset_utils.get_directories_in_directory(cfg['UCF-101'])
    actions = [dataset_utils.get_dir_name_from_path(item) for item in actions]
    objects_json_files = dataset_utils.get_files_in_directory(cfg['UCF-101-objects-json'])
    objects_json_full_path = ['{}\\{}'.format(cfg['UCF-101-objects-json'], item) for item in objects_json_files]
    action_json_dir = {} # actions: [.json files]
    for action in actions:
        action_json_dir[action] = [item for item in objects_json_full_path if action in item]
    print('script ended')
