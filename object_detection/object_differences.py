import json
import math
import os
import pose_detection.pose_utils as pose_utils

import yaml

X1 = 0
Y1 = 1
X2 = 2
Y2 = 3
max_b = 1000

if __name__ == '__main__':
    print('Object detection dynamics of changes')
    dir_path = os.path.dirname(os.path.realpath(__file__))

    with open(dir_path + "/resources/config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    objects = pose_utils.get_files_in_directory(cfg['UCF-101-fail-objects-json-db'])
    object_tables = ['{}\\{}'.format(cfg['UCF-101-fail-objects-json-db'], item) for item in objects]
    for table_file in object_tables:
        print(table_file)
        diff_table = {}
        with open(table_file) as infile:
            table = json.load(infile)
        last_detected = {}
        i = 0
        for id in table:
            entry = table[id]
            name = entry['name']
            if name not in last_detected.keys():
                last_detected[name] = entry
                continue
            prev = last_detected[name]
            name = entry['name']
            d_frame = int(entry['frame']) - int(prev['frame'])
            d_proportion = float(entry['proportion']) / float(prev['proportion'])
            d_x = float(entry['x']) - float(prev['x'])
            d_y = float(entry['y']) - float(prev['y'])
            d_prob = float(entry['probability']) - float(prev['probability'])
            diff_entry = {'name': name,
                          'd_frame': d_frame,
                          'd_proportion': d_proportion,
                          'd_x': d_x,
                          'd_y': d_y,
                          'd_prob': d_prob}
            last_detected[name] = entry
            diff_table[i] = diff_entry
            i = i + 1
        out_file = table_file.replace('UCF-101-fail-objects-json-db', 'UCF-101-fail-objects-json-diff')
        out_file = out_file.replace('-db.json', '-diff.json')
        with open(out_file, 'w') as outfile:
            json.dump(diff_table, outfile)
        print(out_file)
