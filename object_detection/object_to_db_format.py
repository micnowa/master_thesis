import json
import math
import os
import pose_detection.pose_utils as pose_utils

import yaml

X1 = 0
Y1 = 1
X2 = 2
Y2 = 3
max_b = 1000


def a_b_rectangle(box_points):
    x1 = box_points[X1]
    y1 = box_points[Y1]
    x2 = box_points[X2]
    y2 = box_points[Y2]
    a = math.fabs(x2 - x1)
    b = math.fabs(y2 - y1)
    return [a, b]


def coordinates_to_rectangle(box_points):
    [a, b] = a_b_rectangle(box_points)
    if b < 1 / max_b:
        return max_b
    return a / b


def rectangle_size(box_points):
    [a, b] = a_b_rectangle(box_points)
    return a * b


def json_object_into_entry(json_entry):
    dict = {}
    # dict['name'] =


if __name__ == '__main__':
    print('Object db format')
    dir_path = os.path.dirname(os.path.realpath(__file__))

    with open(dir_path + "/resources/config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    objects = pose_utils.get_files_in_directory(cfg['UCF-101-fail-objects-json'])
    objects_full_path = [os.path.join(cfg['UCF-101-fail-objects-json'], item) for item in objects]
    objects_full_path = sorted(objects_full_path)
    for object_file in objects_full_path:
        table = {}
        with open(object_file) as infile:
            json_data = json.load(infile)
            print(infile)
        i = 0
        for frame_num in json_data:
            objects_list = json_data[frame_num]
            for detected_object in objects_list:
                box_points = detected_object['box_points']
                proportion = coordinates_to_rectangle(box_points=box_points)
                entry = {'frame': frame_num, 'name': detected_object['name'], 'proportion': proportion,
                         'x': box_points[X1], 'y': box_points[Y1], 'size': rectangle_size(box_points),
                         'probability': detected_object['percentage_probability']}
                table[i] = entry
                i = i + 1
        file_name = object_file.replace('UCF-101-fail-objects-json', 'UCF-101-fail-objects-json-db')
        file_name = file_name.replace('.json', '-db.json')
        with open(file_name, 'w') as outfile:
            json.dump(table, outfile)
        print(file_name)
