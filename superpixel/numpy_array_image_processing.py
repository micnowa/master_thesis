import numpy as np
from PIL import Image
from skimage.segmentation import slic
from skimage.util import img_as_float

EVERY_FRAME = 10
path = 'C:\\Users\\micha\\Documents\\Informatyka_EiTI_2018-2020\\mgr\\datasets\\sample\\v_Diving_g22_c04_opt.npy'


def segments_to_segment_dict(numpy_array):
    width = numpy_array.shape[0]
    height = numpy_array.shape[0]
    dict = {}
    pixels_number = width * height
    for i in range(0, width):
        for j in range(0, height):
            dict[numpy_array[i][j]] = dict.get(numpy_array[i][j], 0) + 1
    for key in dict:
        dict[key] = dict[key] / pixels_number
    print(dict)
    return dict


def opt_flow_numpy_array_to_image(path):
    npy_array = np.load(path)
    for i in range(0, npy_array.shape[0], EVERY_FRAME):
        current_frame = npy_array[i]
        img = Image.fromarray(current_frame, 'RGB')
        image = img_as_float(img)
        segments = slic(image, sigma=5)
        segment_dict = segments_to_segment_dict(segments)
        print(segment_dict)


opt_flow_numpy_array_to_image(path=path)
