# import the necessary packages
import json
import os
from os import listdir
from os.path import isfile, join

import numpy as np
import yaml
from PIL import Image
from skimage import io
from skimage.segmentation import slic
from skimage.util import img_as_float

JSON_POSITION = 0
NUMPY_POSITION = 1
WIDTH = 320
HEIGHT = 240
X1 = 0
Y1 = 1
X2 = 2
Y2 = 3
BOUND = 5
HUE_NUM = 8
SATURATION_NUM = 8
VALUE_NUM = 8
MAX_FRAMES_NUMBER = 100
FRAMES_STEP = 5

image_path = 'C:\\Users\\micha\\Documents\\Informatyka_EiTI_2018-2020\\mgr\\datasets\\superpixels\\robert-lewadowski-na-tle-pustych-krzeselek.jpg'
image = img_as_float(io.imread(image_path))

EVERY_FRAME = 10
PATH = 'C:\\Users\\micha\\Documents\\Informatyka_EiTI_2018-2020\\mgr\\datasets\\sample\\v_Diving_g22_c04_opt.npy'

possible_frames_list = [str(item) for item in range(1, MAX_FRAMES_NUMBER, FRAMES_STEP)]


def get_directories_in_directory(path):
    var = [dirr[0] for dirr in os.walk(path)]
    var.pop(0)
    return var


def get_npy_files_in_directory(path):
    only_files = [f for f in listdir(path) if isfile(join(path, f)) and join(path, f).__contains__('npy')]
    return only_files


def opt_flow_numpy_array_to_image(object_opt_flow, target_path):
    stat_dict = {}
    for i in object_opt_flow:
        objects_arrays = object_opt_flow[i]
        for object in objects_arrays:
            current_frame = objects_arrays[object]
            print(object)
            img = Image.fromarray(current_frame, 'RGB')
            image = img_as_float(img)
            segments = slic(image, sigma=5)
            segment_dict = segments_to_segment_dict(segments)
            print(segment_dict)
            stat_array = numpy_array_statistics(segment_dict)
            stat_dict[i] = stat_array

    # json_object = json.dumps(stat_dict)
    # f = open(target_path, "w")
    # f.write(json_object)
    # f.close()


def segments_to_segment_dict(numpy_array):
    dict = {}
    pixels_number = numpy_array.shape[0] * numpy_array.shape[1]
    for i in range(0, numpy_array.shape[0]):
        for j in range(0, numpy_array.shape[1]):
            dict[numpy_array[i][j]] = dict.get(numpy_array[i][j], 0) + 1
    for key in dict:
        dict[key] = dict[key] / pixels_number
    return dict


def numpy_array_statistics(dict):
    array = []
    for key in dict:
        array.append(dict[key])
    length = len(array)
    average = np.mean(array)
    std_dev = np.std(array)
    return {'length': length,
            'average': average,
            'std_dec': std_dev}


def opt_flow_for_video_with_boundaries(table, bound):
    json_data, npy_array = load_json_and_npy_array(table)
    object_opt_flow = {}
    for frame_number in json_data:
        objects = json_data[frame_number]
        try:
            frame = npy_array[int(frame_number)]
        except IndexError:
            print(IndexError.__name__)
            continue
        for obj in objects:
            object_opt_flow[frame_number] = {}
            x1, y1, x2, y2 = extend_box_points(obj['box_points'], bound)
            _sample = frame[y1:y2, x1:x2]
            object_opt_flow[frame_number][obj['name']] = _sample
    return object_opt_flow


def extend_box_points(box_points, bound):
    d_x1y1 = box_points[X1] * box_points[X1] + box_points[Y1] * box_points[Y1]
    d_x2y2 = box_points[X2] * box_points[X2] + box_points[Y2] * box_points[Y2]
    if d_x1y1 >= d_x2y2:
        return [0, 0, 0, 0]
    x1 = box_points[X1] - bound
    y1 = box_points[Y1] - bound
    x2 = box_points[X2] + bound
    y2 = box_points[Y2] + bound
    x1 = box_point_within_range(int(x1), WIDTH)
    y1 = box_point_within_range(int(y1), HEIGHT)
    x2 = box_point_within_range(int(x2), WIDTH)
    y2 = box_point_within_range(int(y2), HEIGHT)
    return [x1, y1, x2, y2]


def box_point_within_range(box_point, dimension):
    if box_point < 0:
        return 0
    elif box_point > dimension:
        return dimension - 1
    else:
        return box_point


def load_json_and_npy_array(table):
    video_path = table[NUMPY_POSITION]
    json_path = table[JSON_POSITION]
    with open(json_path) as json_file:
        json_data = json.load(json_file)
    npy_array = np.load(video_path)
    return [json_data, npy_array]


def get_directories_in_directory(path):
    var = [dirr[0] for dirr in os.walk(path)]
    var.pop(0)
    return var


def get_dir_name_from_path(path):
    return os.path.basename(os.path.normpath(path))


def get_files_in_directory(path):
    only_files = [f for f in listdir(path) if isfile(join(path, f))]
    return only_files


if __name__ == '__main__':
    print('Object optical flow')
    dir_path = os.path.dirname(os.path.realpath(__file__))

    with open(dir_path + "/resources/config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    actions = get_directories_in_directory(cfg['UCF-101-opt'])
    actions = [get_dir_name_from_path(item) for item in actions]
    files_names = get_files_in_directory(cfg['UCF-101-objects-json-scaled'])
    files_full_path = [os.path.join(cfg['UCF-101-objects-json-scaled'], item) for item in files_names]
    files_names = [item.replace('-scaled.json', '') for item in files_names]
    actions_dict = {}
    for action in actions:
        actions_dict[action] = [[os.path.join(cfg['UCF-101-objects-json-scaled'], '{}-scaled.json'.format(item)),
                                 os.path.join(cfg['UCF-101-opt'], action, '{}.npy'.format(item))]
                                for item in files_names if action in item]

    for action in actions_dict:
        for tab in actions_dict[action]:
            print(tab[JSON_POSITION])
            print(tab[NUMPY_POSITION])
            string = tab[0]
            statistics_dict = {}
            frame_dict = opt_flow_for_video_with_boundaries(tab, BOUND)
            frame_dict = {key: val for key, val in frame_dict.items() if key in possible_frames_list}
            result = opt_flow_numpy_array_to_image(frame_dict, tab[JSON_POSITION])
            print(result)
