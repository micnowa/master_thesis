import json
import os
import pickle

import numpy as np
import yaml

from object_optical_flow.array_object_processing import hue_value_saturation_dictionary_for_frame, \
    frame_saturation_and_hue
from object_optical_flow.array_object_statistics import distances_dictionary_for_all, \
    statistics_measures_for_distance_dictionary
from object_optical_flow.object_optical_flow_utils import box_point_within_range
from pose_detection import pose_utils

JSON_POSITION = 0
VIDEO_POSITION = 1
WIDTH = 320
HEIGHT = 240
X1 = 0
Y1 = 1
X2 = 2
Y2 = 3
BOUND = 5
HUE_NUM = 8
SATURATION_NUM = 8
VALUE_NUM = 8
MAX_FRAMES_NUMBER = 100
FRAMES_STEP = 10

possible_frames_list = [str(item) for item in range(1, MAX_FRAMES_NUMBER, 10)]


def opt_flow_for_video_with_boundaries(table, bound):
    json_data, npy_array = load_json_and_npy_array(table)
    object_opt_flow = {}
    for frame_number in json_data:
        objects = json_data[frame_number]
        try:
            frame = npy_array[int(frame_number)]
        except IndexError:
            print(IndexError.__name__)
            continue
        for obj in objects:
            object_opt_flow[frame_number] = {}
            x1, y1, x2, y2 = extend_box_points(obj['box_points'], bound)
            _sample = frame[y1:y2, x1:x2]
            object_opt_flow[frame_number][obj['name']] = _sample
    return object_opt_flow


def concatenate_object_in_frame(frame, box_points):
    if all(v == 0 for v in box_points):
        return frame
    x1, y1, x2, y2 = box_points
    sample = frame[y1:(y2 + 1), x1:(x2 + 1)]
    return sample


def extend_box_points(box_points, bound):
    d_x1y1 = box_points[X1] * box_points[X1] + box_points[Y1] * box_points[Y1]
    d_x2y2 = box_points[X2] * box_points[X2] + box_points[Y2] * box_points[Y2]
    if d_x1y1 >= d_x2y2:
        return [0, 0, 0, 0]
    x1 = box_points[X1] - bound
    y1 = box_points[Y1] - bound
    x2 = box_points[X2] + bound
    y2 = box_points[Y2] + bound
    x1 = box_point_within_range(int(x1), WIDTH)
    y1 = box_point_within_range(int(y1), HEIGHT)
    x2 = box_point_within_range(int(x2), WIDTH)
    y2 = box_point_within_range(int(y2), HEIGHT)
    return [x1, y1, x2, y2]


def load_json_and_npy_array(table):
    video_path = table[VIDEO_POSITION]
    json_path = table[JSON_POSITION]
    with open(json_path) as json_file:
        json_data = json.load(json_file)
    npy_array = np.load(video_path)
    return [json_data, npy_array]


def save_dictionary(obj_dict, directory, name):
    with open(os.path.join(directory, '{}.pkl'.format(name)), 'wb') as f:
        pickle.dump(obj_dict, f)


if __name__ == '__main__':
    print('Object optical flow')
    dir_path = os.path.dirname(os.path.realpath(__file__))

    with open(dir_path + "/resources/config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    actions = pose_utils.get_directories_in_directory(cfg['UCF-101-opt'])
    actions = [pose_utils.get_dir_name_from_path(item) for item in actions]
    files_names = pose_utils.get_files_in_directory(cfg['UCF-101-objects-json-scaled'])
    files_full_path = [os.path.join(cfg['UCF-101-objects-json-scaled'], item) for item in files_names]
    files_names = [item.replace('-scaled.json', '') for item in files_names]
    actions_dict = {}
    for action in actions:
        actions_dict[action] = [[os.path.join(cfg['UCF-101-objects-json-scaled'], '{}-scaled.json'.format(item)),
                                 os.path.join(cfg['UCF-101-opt'], action, '{}.npy'.format(item))]
                                for item in files_names if action in item]

    for action in actions_dict:
        for tab in actions_dict[action]:
            print(tab[JSON_POSITION])
            print(tab[VIDEO_POSITION])
            string = tab[0]
            statistics_dict = {}
            frame_dict = opt_flow_for_video_with_boundaries(tab, BOUND)
            frame_dict = {key: val for key, val in frame_dict.items() if key in possible_frames_list}
            full_hue_statistics = {key: None for key, value in frame_dict.items()}
            full_saturation_statistics = {key: None for key, value in frame_dict.items()}
            full_value_statistics = {key: None for key, value in frame_dict.items()}
            for frame_num in frame_dict:
                objects_frames_for_one_frame = frame_dict[frame_num]
                one_frame_hue_statistics = {}
                one_frame_saturation_statistics = {}
                one_frame_value_statistics = {}
                for object_name in objects_frames_for_one_frame:
                    object_frame = objects_frames_for_one_frame[object_name]
                    h_s_v_frame = frame_saturation_and_hue(object_frame)
                    hue_dict, saturation_dict, value_dict = hue_value_saturation_dictionary_for_frame(frame=h_s_v_frame,
                                                                                                      hue_num=HUE_NUM,
                                                                                                      sat_num=SATURATION_NUM,
                                                                                                      val_num=VALUE_NUM)
                    hue_dict_numbers = {key: len(tab) for key, tab in hue_dict.items()}
                    saturation_dict_numbers = {key: len(tab) for key, tab in saturation_dict.items()}
                    value_dict_numbers = {key: len(tab) for key, tab in value_dict.items()}

                    hue_distances = distances_dictionary_for_all(dictionary=hue_dict)
                    saturation_distances = distances_dictionary_for_all(dictionary=saturation_dict)
                    value_distances = distances_dictionary_for_all(dictionary=value_dict)

                    hue_statistics = statistics_measures_for_distance_dictionary(dictionary=hue_distances,
                                                                                 numbers=hue_dict_numbers)
                    saturation_statistics = statistics_measures_for_distance_dictionary(dictionary=saturation_dict,
                                                                                        numbers=saturation_dict_numbers)
                    value_statistics = statistics_measures_for_distance_dictionary(dictionary=value_dict,
                                                                                   numbers=value_dict_numbers)
                    one_frame_hue_statistics[object_name] = hue_statistics
                    one_frame_saturation_statistics[object_name] = saturation_statistics
                    one_frame_value_statistics[object_name] = value_statistics
                full_hue_statistics[frame_num] = one_frame_hue_statistics
                full_saturation_statistics[frame_num] = one_frame_saturation_statistics
                full_value_statistics[frame_num] = one_frame_value_statistics

            hue_saturation_value_dict = {'hue': full_hue_statistics,
                                         'saturation': full_saturation_statistics,
                                         'value': full_value_statistics}

            json_name = tab[0].replace('UCF-101-objects-json-scaled', 'UCF-101-object-opt-flow')
            json_name = json_name.replace('UCF-101-objects-json-scaled', 'UCF-101-object-opt-flow')
            json_name = json_name.replace('-scaled.json', '-object-opt-flow.json')

            json_data = json.dumps(hue_saturation_value_dict)
            f = open(json_name, "w")
            f.write(json_data)
            print(json_name)
