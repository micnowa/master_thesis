import math

import numpy as np
import random

from object_optical_flow.array_object_processing import DIMENSIONS

NUMBER_OF_RANDOM_ELEMENTS = 100
X_POSITION = 0
Y_POSITION = 1

X_MIN_POSITION = 0
Y_MIN_POSITION = 1
X_MAX_POSITION = 2
Y_MAX_POSITION = 3


class ExtremePoints:
    def __init__(self, x_min, y_min, x_max, y_max):
        self.x_min = x_min
        self.y_min = y_min
        self.x_max = x_max
        self.y_max = y_max

    def area(self):
        return abs(self.x_max - self.x_min) * abs(self.y_max - self.y_min)

    def x_to_y_ratio(self):
        if math.isclose(self.y_max, self.y_min):
            return math.nan
        return abs(self.x_max - self.x_min) / abs(self.y_max - self.y_min)

    def is_existent(self):
        for attr, value in self.__dict__.items():
            if value is math.nan:
                return False
        return True

    def to_table(self):
        return [self.x_min, self.y_min, self.x_max, self.y_max]

    def compare(self, extreme_points):
        tab1 = self.to_table()
        tab2 = extreme_points.to_table()
        if sum(tab1) is math.nan and sum(tab2) is math.nan:
            return True
        for i in range(0, len(tab1)):
            if not math.isclose(tab1[i], tab2[i]):
                return False
        return True


class DistanceStatistics:
    def __init__(self, min, max, avg, std, num):
        self.min = min
        self.max = max
        self.avg = avg
        self.std = std
        self.num = num

    def to_dictionary(self):
        return {'min': self.min, 'max': self.max, 'avg': self.avg, 'std': self.std, 'num': self.num}


def array_average(np_array):
    avg = np.average(np_array)
    return avg


def optical_flow_frame_average(frame):
    avg = []
    for i in range(0, DIMENSIONS):
        avg.append(np.average(frame[:, :, i]))
    return np.array(avg)


def optical_flow_frame_standard_deviation(frame):
    avg = []
    for i in range(0, DIMENSIONS):
        avg.append(np.std(frame[:, :, i]))
    return np.array(avg)


def distances_dictionary_for_all(dictionary):
    dictionary_copy = {key: random.sample(val, NUMBER_OF_RANDOM_ELEMENTS) for key, val in dictionary.items() if
                       len(val) > NUMBER_OF_RANDOM_ELEMENTS}
    distances_dictionary = {}
    for key in dictionary_copy:
        table = dictionary_copy[key]
        if len(table) is 0 or len(table) is 1:
            distances_dictionary[key] = 0
            continue
        table_distance = []
        for i in range(0, len(table)):
            for j in range(i + 1, len(table)):
                tab1 = table[i]
                tab2 = table[j]
                x1, y1 = tab1[:]
                x2, y2 = tab2[:]
                table_distance.append(math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2))
        distances_dictionary[key] = table_distance
    return distances_dictionary


def extreme_points_for_points_in_range(dictionary):
    max_points_dict = {}
    for key in dictionary:
        table = dictionary[key]
        if not table:
            max_points_dict[key] = ExtremePoints(x_min=math.nan,
                                                 y_min=math.nan,
                                                 x_max=math.nan,
                                                 y_max=math.nan)
            continue
        x_max = - math.inf
        y_max = - math.inf
        x_min = math.inf
        y_min = math.inf
        for point in table:
            if point[X_POSITION] > x_max:
                x_max = point[X_POSITION]
            if point[X_POSITION] < x_min:
                x_min = point[X_POSITION]
            if point[Y_POSITION] > y_max:
                y_max = point[Y_POSITION]
            if point[Y_POSITION] < y_min:
                y_min = point[Y_POSITION]
        max_points_dict[key] = ExtremePoints(x_min=x_min,
                                             y_min=y_min,
                                             x_max=x_max,
                                             y_max=y_max)
    return max_points_dict


def statistics_measures_for_distance_dictionary(dictionary, numbers=None):
    if numbers is None:
        numbers = {key: 0 for key in dictionary.keys()}
    statistics_dict = {}
    for key in dictionary:
        table = dictionary[key]
        if not table:
            statistics_dict[key] = DistanceStatistics(min=-1,
                                                      max=-1,
                                                      avg=-1,
                                                      std=-1,
                                                      num=-1).to_dictionary()
        else:
            statistics_dict[key] = DistanceStatistics(min=np.min(table),
                                                      max=np.max(table),
                                                      avg=np.average(table),
                                                      std=np.std(table),
                                                      num=numbers[key]).to_dictionary()
    return statistics_dict
