import json
import os
import numpy as np
import yaml

from pose_detection import pose_utils

X1 = 0
Y1 = 1
X2 = 2
Y2 = 3
WIDTH = 320
HEIGHT = 240


def scale_object_json(json_data):
    for frame_num in json_data:
        entry = json_data[frame_num]
        for detection in entry:
            box_points = detection['box_points']
            for i in range(0, len(box_points)):
                box_points[i] = 1 - box_points[i]
            detection['box_points'] = box_points


def opt_flow_box_points_from_json_data(json_data, width, height):
    for frame_num in json_data:
        entry = json_data[frame_num]
        for detection in entry:
            box_points = detection['box_points']
            box_points[X1] = box_point_within_range(int((box_points[X1]) * width), width)
            box_points[Y1] = box_point_within_range(int((box_points[Y1]) * height), height)
            box_points[X2] = box_point_within_range(int((box_points[X2]) * width), width)
            box_points[Y2] = box_point_within_range(int((box_points[Y2]) * height), height)
            # scale_box_points(box_points)
            detection['box_points'] = box_points


def box_point_within_range(box_point, dimension):
    if box_point < 0:
        return 0
    elif box_point > dimension:
        return dimension - 1
    else:
        return box_point


def scale_box_points(box_points):
    tmp = box_points[Y1]
    box_points[Y1] = box_points[Y2]
    box_points[Y2] = tmp


if __name__ == '__main__':
    print('Object optical flow')
    dir_path = os.path.dirname(os.path.realpath(__file__))

    with open(dir_path + "/resources/config.yaml") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    objects = pose_utils.get_files_in_directory(cfg['UCF-101-objects-json'])
    objects_full_path = ['{}\\{}'.format(cfg['UCF-101-objects-json'], item) for item in objects]
    objects_full_path = sorted(objects_full_path)
    for object_file in objects_full_path:
        with open(object_file) as infile:
            json_data = json.load(infile)
            print(infile)
        opt_flow_box_points_from_json_data(json_data=json_data,
                                           width=WIDTH,
                                           height=HEIGHT)
        file_name = object_file.replace('UCF-101-objects-json', 'UCF-101-objects-json-scaled')
        file_name = file_name.replace('.json', '-scaled.json')
        with open(file_name, 'w') as outfile:
            json.dump(json_data, outfile)
        print(file_name)
