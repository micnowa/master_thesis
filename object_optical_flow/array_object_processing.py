import colorsys
import math

import numpy as np
import scipy
from scipy import ndimage

DIMENSIONS = 3
HEIGHT_POSITION = 0
WIDTH_POSITION = 1
RGB_POSITION = 2
HUE_POSITION = 0
SATURATION_POSITION = 1
MAX_HUE = 1
MAX_SATURATION = 1
MAX_VALUE = 1
VALUE_POSITION = 2


def apply_gaussian_filter(frame):
    np_array = np.copy(frame)
    for i in range(0, DIMENSIONS):
        np_array[:, :, i] = scipy.ndimage.gaussian_filter(input=frame[:, :, i], sigma=1)
    return np_array


def frame_saturation_and_hue(frame):
    shape = frame.shape
    height = shape[HEIGHT_POSITION]
    width = shape[WIDTH_POSITION]
    saturation_frame = np.empty(shape=(shape[HEIGHT_POSITION], shape[WIDTH_POSITION], 3))
    for i in range(0, height):
        for j in range(0, width):
            h, s, v = rgb_to_hue_and_saturation(frame[i, j, :])
            saturation_frame[i, j, :] = [h, s, v]
    return saturation_frame


def rgb_to_hue_and_saturation(tab):
    r, g, b = tab[:]
    hue, saturation, value = colorsys.rgb_to_hsv(r, g, b)
    return hue, saturation, value


def hue_value_saturation_dictionary_for_frame(frame, hue_num, sat_num, val_num):
    shape = frame.shape
    height = shape[HEIGHT_POSITION]
    width = shape[WIDTH_POSITION]
    hue_ranges = create_ranges(num=hue_num, max_value=MAX_HUE)
    saturation_ranges = create_ranges(num=sat_num, max_value=MAX_SATURATION)
    value_ranges = create_ranges(num=val_num, max_value=MAX_VALUE)
    hues_frame = np.copy(frame[:, :, HUE_POSITION])
    saturation_frame = np.copy(frame[:, :, SATURATION_POSITION])
    value_frame = np.copy(frame[:, :, VALUE_POSITION])

    hue_coordinates_dict = {}
    for i in range(0, len(hue_ranges)):
        hue_coordinates_dict[i] = []

    saturation_coordinates_dict = {}
    for i in range(0, len(saturation_ranges)):
        saturation_coordinates_dict[i] = []

    value_coordinates_dict = {}
    for i in range(0, len(value_ranges)):
        value_coordinates_dict[i] = []

    for i in range(0, height):
        for j in range(0, width):
            hue = hues_frame[i, j]
            saturation = saturation_frame[i, j]
            value = value_frame[i, j]

            hue_index = get_index_for_range(hue_ranges, hue)
            saturation_index = get_index_for_range(saturation_ranges, saturation)
            value_index = get_index_for_range(value_ranges, value)
            if hue_index is -1 or saturation_index is -1:
                continue

            x, y = i / height, j / width
            hue_coordinates_dict[hue_index].append((x, y))
            saturation_coordinates_dict[saturation_index].append((x, y))
            value_coordinates_dict[saturation_index].append((x, y))

    return hue_coordinates_dict, saturation_coordinates_dict, value_coordinates_dict


def create_ranges(num, max_value, offset=0):
    if num <= 0:
        return []
    ranges = []
    for i in range(1, num + 1):
        ranges.append(max_value / num * i)
    return ranges


def get_index_for_range(ranges, number):
    for i in range(0, len(ranges)):
        if number <= ranges[i]:
            return i
    return -1
