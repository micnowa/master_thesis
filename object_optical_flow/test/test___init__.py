from unittest import TestCase

from object_optical_flow import opt_flow_for_video_with_boundaries, extend_box_points
from object_optical_flow.test.test_utils import _BALANCE_BEAM_JSON, _BALANCE_BEAM_NPY

_BOUND = 5


class Test(TestCase):
    def test_opt_flow_for_video_with_boundaries(self):
        result = opt_flow_for_video_with_boundaries([_BALANCE_BEAM_JSON, _BALANCE_BEAM_NPY], _BOUND)
        print('s')

    def test_extend_box_points_for_limit_values(self):
        box_points = [0, 0, 319, 239]
        result = extend_box_points(box_points=box_points,
                                   bound=0)
        self.assertEquals([0, 0, 319, 239], result)

    def test_extend_box_points_for_close_to_limit_values(self):
        box_points = [2, 3, 317, 237]
        result = extend_box_points(box_points=box_points,
                                   bound=10)
        self.assertEquals([0, 0, 319, 239], result)

    def test_extend_box_points_for_average_values(self):
        box_points = [100, 100, 200, 200]
        result = extend_box_points(box_points=box_points,
                                   bound=10)
        self.assertEquals([90, 90, 210, 210], result)

    def test_extend_box_points_for_xy1_greater_than_xy2_values(self):
        result = extend_box_points(box_points=[200, 200, 100, 100],
                                   bound=10)
        self.assertEquals([0, 0, 0, 0], result)
