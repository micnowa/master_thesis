import json
import numpy as np
import os

_BALANCE_BEAM_NPY = os.path.join('resources', 'BalanceBeam', 'v_BalanceBeam_g01_c04.npy')
_BALANCE_BEAM_JSON = os.path.join('resources', 'BalanceBeam', 'v_BalanceBeam_g01_c04-scaled.json')


def load_balance_beam_data():
    npy_array = np.load(_BALANCE_BEAM_NPY)
    with open(_BALANCE_BEAM_JSON) as json_file:
        json_data = json.load(json_file)
    return [json_data, npy_array]
