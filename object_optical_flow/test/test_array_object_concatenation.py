import math
from unittest import TestCase

import numpy as np

from object_optical_flow import concatenate_object_in_frame
from object_optical_flow.array_object_processing import create_ranges, hue_value_saturation_dictionary_for_frame

basic_array = np.array([[0, 0, 0, 0, 0, 0],
                        [0, 0, 1, 1, 0, 0],
                        [0, 1, 4, 4, 1, 0],
                        [0, 1, 4, 4, 1, 0],
                        [0, 0, 1, 1, 0, 0],
                        [0, 0, 0, 0, 0, 0]])


def create_input3d_array(array):
    new_input3d_array = np.ndarray([6, 6, 3])
    for i in range(0, 3):
        new_input3d_array[:, :, i] = array
    return new_input3d_array


def create_3d_array_from_1d_array(array):
    shape = array.shape
    d1 = shape[0]
    d2 = shape[1]
    new_shape = (d1, d2, 3)
    new_input3d_array = np.empty(shape=new_shape)
    for i in range(0, 3):
        new_input3d_array[:, :, i] = array
    return new_input3d_array


class ObjectConcatenationTest(TestCase):
    input_array = basic_array
    central_array = np.array([[0, 1, 1, 0],
                              [1, 4, 4, 1],
                              [1, 4, 4, 1],
                              [0, 1, 1, 0]])
    top_left_corner_array = np.array([[0, 0, 0],
                                      [0, 0, 1],
                                      [0, 1, 4]])
    bottom_right_corner_array = np.array([[4, 1, 0],
                                          [1, 0, 0],
                                          [0, 0, 0]])

    input3D_array = create_input3d_array(basic_array)

    output3D_central_array = np.ndarray([4, 4, 3])
    for i in range(0, 3):
        output3D_central_array[:, :, i] = central_array

    def test_concatenate_object_in_frame(self):
        result_array = concatenate_object_in_frame(frame=self.input_array,
                                                   box_points=[1, 1, 4, 4])
        np.testing.assert_array_equal(self.central_array, result_array)

    def test_do_not_concatenate_if_object_is_not_present(self):
        result_array = concatenate_object_in_frame(frame=self.input_array,
                                                   box_points=[0, 0, 0, 0])
        np.testing.assert_array_equal(self.input_array, result_array)

    def test_concatenate_object_in_top_left_corner(self):
        result_array = concatenate_object_in_frame(frame=self.input_array,
                                                   box_points=[0, 0, 2, 2])
        np.testing.assert_array_equal(self.top_left_corner_array, result_array)

    def test_concatenate_object_in_bottom_right_corner(self):
        result_array = concatenate_object_in_frame(frame=self.input_array,
                                                   box_points=[3, 3, 5, 5])
        np.testing.assert_array_equal(self.bottom_right_corner_array, result_array)

    def test_3D_array_object_central_concatenation(self):
        result_array = concatenate_object_in_frame(frame=self.input3D_array,
                                                   box_points=[1, 1, 4, 4])
        np.testing.assert_array_equal(self.output3D_central_array, result_array)
