import math
from unittest import TestCase

import numpy as np

from object_optical_flow.array_object_statistics import array_average, optical_flow_frame_average, \
    optical_flow_frame_standard_deviation, distances_dictionary_for_all, extreme_points_for_points_in_range, \
    ExtremePoints, statistics_measures_for_distance_dictionary
from object_optical_flow.test.test_array_object_concatenation import create_input3d_array


class Test(TestCase):
    ones_array_1d = np.ones(6)
    ones_array_2d = np.ones(shape=(6, 6))

    matrix = np.array([[0, 0, 1, 1, 0, 0],
                       [0, 0, 2, 2, 0, 0],
                       [1, 2, 4, 4, 2, 1],
                       [1, 2, 4, 4, 2, 1],
                       [0, 0, 2, 2, 0, 0],
                       [0, 0, 1, 1, 0, 0]])

    matrix_3d = create_input3d_array(matrix)

    hue_dict = {1: [(0, 0), (0.25, 0.25), (0.5, 0.5)],
                2: [(0.1, 0.1), (0.15, 0.15), (0.12, 0.12), (0.13, 0.13)],
                3: [],
                4: [(0.5, 0.5)],
                5: []}

    def test_array_average_1d_array(self):
        result = array_average(self.ones_array_1d)
        self.assertEquals(1, result)

    def test_array_average_2d_array(self):
        result = array_average(self.ones_array_2d)
        self.assertEquals(1, result)

    def test_array_average_matrix(self):
        result = array_average(self.ones_array_2d)
        self.assertAlmostEqual(1.0, result)

    def test_optical_flow_average_matrix_3d(self):
        result = optical_flow_frame_average(self.matrix_3d)
        np.testing.assert_array_almost_equal(np.array([1.1111111, 1.1111111, 1.1111111]), result)

    def test_optical_flow_standard_deviation_matrix_3d(self):
        result = optical_flow_frame_standard_deviation(self.matrix_3d)
        self.assertAlmostEqual(1.0, result)

    def test_distances_for_all(self):
        result = distances_dictionary_for_all(self.hue_dict).values()
        np.testing.assert_array_almost_equal([0, 0], [0, 0])

    def test_extreme_points_for_all(self):
        result = extreme_points_for_points_in_range(self.hue_dict)
        extreme_points_dict = {1: ExtremePoints(x_min=0, y_min=0, x_max=0.5, y_max=0.5),
                               2: ExtremePoints(x_min=0.1, y_min=0.1, x_max=0.15, y_max=0.15),
                               3: ExtremePoints(math.nan, math.nan, math.nan, math.nan),
                               4: ExtremePoints(x_min=0.5, y_min=0.5, x_max=0.5, y_max=0.5),
                               5: ExtremePoints(math.nan, math.nan, math.nan, math.nan)}
        comparison_tab = []
        for i in range(1, len(extreme_points_dict) + 1):
            comparison_tab.append(extreme_points_dict[i].compare(result[i]))
        self.assertAlmostEqual(comparison_tab, [True, True, True, True, True])

    def test_statistics_measures_distance_dictionary(self):
        extreme_points = extreme_points_for_points_in_range(self.hue_dict)
        distances = distances_dictionary_for_all(self.hue_dict)
        min_max_dictionary = statistics_measures_for_distance_dictionary(distances)
        self.assertAlmostEqual(extreme_points, distances)