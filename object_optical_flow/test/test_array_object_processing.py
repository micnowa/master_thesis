import math
from unittest import TestCase

import numpy as np

from object_optical_flow.array_object_processing import apply_gaussian_filter, frame_saturation_and_hue, create_ranges, \
    hue_value_saturation_dictionary_for_frame
from object_optical_flow.test.test_array_object_concatenation import create_input3d_array, create_3d_array_from_1d_array


class Test(TestCase):
    ones_array_1d = np.ones(6)
    ones_array_2d = np.ones(shape=(6, 6))

    basic_matrix = np.array([[1, 0, 1, 1, 0, 1],
                             [0, 0, 2, 2, 0, 0],
                             [1, 2, 4, 4, 2, 1],
                             [1, 2, 4, 4, 2, 1],
                             [0, 0, 2, 2, 0, 0],
                             [0, 0, 1, 1, 0, 0]])

    saturation_matrix = np.array([[128, 255, 200, 255, 0, 0],
                                  [100, 0, 72, 20, 0, 200],
                                  [1, 200, 34, 40, 199, 100],
                                  [128, 255, 200, 255, 0, 0],
                                  [100, 0, 72, 20, 0, 200],
                                  [1, 200, 34, 40, 199, 100]])

    matrix_3d = create_input3d_array(basic_matrix)
    saturation_matrix_3d = create_3d_array_from_1d_array(saturation_matrix)

    hue_frame = np.array([[0.5, 1.2, 3.14159],
                          [6.01, 4.0, 0.01],
                          [3.5, 2.9, 5.9]])

    saturation_frame = np.array([[200, 200, 200],
                                 [100, 100, 100],
                                 [40, 30, 20]])

    def test_apply_filter_for_matrix_3d(self):
        result = apply_gaussian_filter(self.matrix_3d)
        self.assertAlmostEqual(1.0, result)

    def test_saturation_for_matrix_255(self):
        result = frame_saturation_and_hue(self.saturation_matrix_3d)
        np.testing.assert_array_almost_equal(np.array([1.1111111, 1.1111111, 1.1111111]), result)

    def test_create_ranges_for_hue(self):
        n = 10
        result = create_ranges(num=n, max_value=2 * math.pi)
        self.assertEquals(10, len(result))

    def test_aggregate_hues_and_saturation_for_single_frame(self):
        n = 10
        hue_and_saturation_frame = np.empty(shape=(3, 3, 2))
        hue_and_saturation_frame[:, :, 0] = self.hue_frame
        hue_and_saturation_frame[:, :, 1] = self.saturation_frame
        print(hue_and_saturation_frame)
        h_dict, s_dict = hue_value_saturation_dictionary_for_frame(hue_and_saturation_frame, 8, 10)
        self.assertEquals(1, 1)
