import json
import os
from unittest import TestCase

from PIL import Image, ImageDraw

from object_optical_flow.object_optical_flow_utils import opt_flow_box_points_from_json_data
from object_optical_flow.object_optical_flow_utils import scale_object_json
from object_optical_flow.test.test_utils import load_balance_beam_data


class Test(TestCase):
    norm_scale_dict = {}

    def test_scale_object_json(self):
        with open(os.path.join(os.getcwd(), 'resources\\test_data.json')) as json_file_path:
            json_data = json.load(json_file_path)
        with open(os.path.join(os.getcwd(), 'resources\\norm_scale_result.json')) as json_file_path:
            result_data = json.load(json_file_path)
        scale_object_json(json_data)
        print(json_data)
        print(result_data)
        for frame_num1, frame_num2 in zip(json_data, result_data):
            entry1 = json_data[frame_num1]
            entry2 = json_data[frame_num2]
            for detection1, detection2 in zip(entry1, entry2):
                self.assertAlmostEqual(detection1['box_points'], detection2['box_points'])

    def test_opt_flow_box_points_from_json_data(self):
        with open(os.path.join(os.getcwd(), 'resources\\test_data.json')) as json_file_path:
            json_data = json.load(json_file_path)
        with open(os.path.join(os.getcwd(), 'resources\\full_scale_result.json')) as json_file_path:
            result_data = json.load(json_file_path)
        opt_flow_box_points_from_json_data(json_data, width=320, height=240)
        for frame_num1, frame_num2 in zip(json_data, result_data):
            entry1 = json_data[frame_num1]
            entry2 = json_data[frame_num2]
            for detection1, detection2 in zip(entry1, entry2):
                self.assertEqual(detection1['box_points'], detection2['box_points'])

    def test_image_print_from_npy_array(self):
        json_data, npy_array = load_balance_beam_data()
        first_image = npy_array[0]
        img = Image.fromarray(first_image, 'RGB')
        print(json_data)
        for i in range(4, len(npy_array)):
            img = Image.fromarray(npy_array[i], 'RGB')
            img1 = ImageDraw.Draw(img)
            table = json_data[str(i + 1)]
            entry = table[0]
            box_points = entry['box_points']
            img1.rectangle(box_points, outline="red")
            img.show()



