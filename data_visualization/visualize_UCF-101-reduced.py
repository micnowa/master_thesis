import json
import numpy as np
import matplotlib.pyplot as plt
import yaml

with open('resources/config.yaml') as yamlfile:
    config = yaml.load(yamlfile)

json_file_path = config['action_dict']

with open(json_file_path) as json_file:
    json_data = json.load(json_file)

print(json_data)

names = list(json_data.keys())
values = list(json_data.values())
print(names)
print(values)
print('Avg: {}'.format(np.average(values)))
print('Std: {}'.format(np.std(values)))
print('Max: {}'.format(np.max(values)))
print('Min: {}'.format(np.min(values)))


plt.plot(names, values)
plt.suptitle('Categorical Plotting')
plt.show()